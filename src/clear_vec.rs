use std::convert::TryInto;
use std::cmp::min;
use crate::mangler::hash_clear_vec;
use crate::convert::{clear_byte_val, clear_from_val};

pub const CAPACITY: usize = 31;

#[derive(Copy, Clone, Ord, PartialOrd, Eq, PartialEq, Hash)]
pub struct ClearVec {
  pub len: u8,
  pub bytes: [u8; CAPACITY],
}

impl ClearVec {
  pub fn from_slice_le(b: &[u8]) -> Self {
    let len: usize = min(b.len(), CAPACITY);
    let mut bytes: [u8; CAPACITY] = [0u8; CAPACITY];
    bytes[0..len].copy_from_slice(b);
    Self { len: len.try_into().unwrap(), bytes }
  }

  pub fn from_str(s: &str) -> ClearVec {
    let input_bytes: &[u8] = s.as_bytes();
    let len: usize = min(input_bytes.len(), CAPACITY);
    let mut bytes: [u8; CAPACITY] = [0u8; CAPACITY];
    for i in 0..len {
      bytes[len - 1 - i] = clear_byte_val(input_bytes[i]);
    }

    ClearVec { len: len.try_into().unwrap(), bytes }
  }

  pub fn obf_hash(self) -> u32 {
    hash_clear_vec(self)
  }

  pub fn capitalize(self) -> ClearVec {
    let mut bytes: [u8; CAPACITY] = self.bytes;
    let len: usize = self.len.into();
    if len > 0 {
      bytes[len - 1] = match bytes[len - 1] {
        b @ 1..=26 => b + 26,
        b => b,
      };
    }
    ClearVec { len: self.len, bytes }
  }

  pub fn upper(self) -> ClearVec {
    let mut bytes: [u8; CAPACITY] = self.bytes;
    for i in 0..usize::from(self.len) {
      bytes[i] = match bytes[i] {
        b @ 1..=26 => b + 26,
        b => b,
      };
    }
    ClearVec { len: self.len, bytes }
  }

  /// Match `^[A-Z_]*$`
  pub fn is_upper_snake_alpha(self) -> bool {
    for i in 0..usize::from(self.len) {
      match self.bytes[i] {
        _b @ 27..=52 => (),
        _b @ 63 => (),
        _ => return false,
      };
    }
    true
  }

  /// Match `^[a-z]+[A-Z][a-z]*$`
  pub fn is_camel_alpha(self) -> bool {
    let mut last_is_upper = false;
    for i in 0..usize::from(self.len) {
      match self.bytes[i] {
        _b @ 1..=26 => {
          last_is_upper = false;
        },
        _b @ 27..=52 => {
          if i == usize::from(self.len - 1) || last_is_upper {
            return false
          } else {
            last_is_upper = true;
          }
        },
        _ => return false,
      };
    }
    true
  }

  /// Match `^[a-z]+[_][a-z]*$`
  pub fn is_snake_alpha(self) -> bool {
    let mut has_under = false;
    for i in 0..usize::from(self.len) {
      match self.bytes[i] {
        _b @ 1..=26 => (),
        _b @ 63 => {
          if i == usize::from(self.len - 1) || has_under {
            return false
          } else {
            has_under = true;
          }
        },
        _ => return false,
      };
    }
    true
  }

  /// Match `^([A-Z][a-z]*)*$`
  pub fn is_pascal_alpha(self) -> bool {
    let mut upper_sequence: u32 = 0;
    for (i, x) in self.bytes[0..usize::from(self.len)].iter().rev().enumerate() {
      match x {
        _b @ 1..=26 => {
          if i == 0 {
            return false;
          }
          upper_sequence = 0;
        },
        _b @ 27..=52 => {
          upper_sequence += 1;
          if upper_sequence >= 3 {
            return false;
          }
        },
        _ => return false,
      };
    }
    true
  }

  pub fn concat(self, right: ClearVec) -> ClearVec {
    if usize::from(right.len) == CAPACITY || self.len == 0 {
      return right;
    } else if right.len == 0 {
      return self;
    }

    let len: usize = min((right.len + self.len).into(), CAPACITY);
    let mut bytes: [u8; CAPACITY] = right.bytes;
    let self_len: usize = len - usize::from(right.len);
    let self_slice: &[u8] = &self.bytes[0..self_len];
    bytes[right.len.into()..len].copy_from_slice(self_slice);
    ClearVec { len: len.try_into().unwrap(), bytes }
  }
}

impl ::std::fmt::Display for ClearVec {
  fn fmt(&self, f: &mut ::std::fmt::Formatter) -> Result<(), ::std::fmt::Error> {
    let s: String = self.bytes[0..self.len.into()].iter().rev().map(|x| clear_from_val(*x)).collect();
    f.write_str(&s)
  }
}

impl ::std::fmt::Debug for ClearVec {
  fn fmt(&self, f: &mut ::std::fmt::Formatter) -> Result<(), ::std::fmt::Error> {
    ::std::fmt::Debug::fmt(&self.to_string() as &String, f)
  }
}


#[cfg(test)]
mod tests {
  use super::*;

  #[test]
  fn test_to_string() {
    assert_eq!(&ClearVec::from_str("").to_string(), "");
    assert_eq!(&ClearVec::from_str("foo").to_string(), "foo");
  }

  #[test]
  fn test_capitalize() {
    assert_eq!(&ClearVec::from_str("").capitalize().to_string(), "");
    assert_eq!(&ClearVec::from_str("foo").capitalize().to_string(), "Foo");
  }

  #[test]
  fn test_upper() {
    assert_eq!(&ClearVec::from_str("").upper().to_string(), "");
    assert_eq!(&ClearVec::from_str("foo").upper().to_string(), "FOO");
  }

  #[test]
  fn test_concat() {
    let foo = ClearVec::from_str("Foo");
    let bar = ClearVec::from_str("Bar");

    assert_eq!(&ClearVec::concat(foo, bar).to_string(), "FooBar");
  }



  #[test]
  fn test_is_camel_alpha() {
    assert!(ClearVec::from_str("foo").is_camel_alpha());
    assert!(ClearVec::from_str("fooBar").is_camel_alpha());
    assert!(!ClearVec::from_str("FooBar").is_camel_alpha());
  }
}
