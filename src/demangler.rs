use std::convert::TryInto;
use std::convert::TryFrom;
use core::num::NonZeroUsize;
use crate::clear_vec::ClearVec;
use crate::clear_vec::CAPACITY;

/**
 * POW2_31_LOW.reduce((old, acc, i) => old + acc * 51 ** i, 0) === 2 ** 31
 */
// 2³¹ in little-endian low representation
const POW2_31_LOW: [u8; 6] = [26, 35, 49, 21, 11, 6];
// 2³¹ in little-endian high representation
const POW2_31_HIGH: [u8; 6] = [26, 35, 49, 21, 62, 5];

const K: u8 = 51;

/// Modular inverse of K over `Z/2³¹Z`
const K_INV: u32 = 2063268603;

/// Number of clear characters
const N: u8 = 64;

#[derive(Copy, Clone, Ord, PartialOrd, Eq, PartialEq, Hash)]
pub struct SolveOptions {
  pub prefix: ClearVec,
  pub suffix: ClearVec,
  pub max_len: u8,
}

pub fn solve(hash: u32, options: SolveOptions) -> impl Iterator<Item=ClearVec> {
  let left = branch_solve(hash, options, true);
  let right = branch_solve(hash, options, false);
  itertools::interleave(left, right)
}

struct Solver {
  options: SolveOptions,
  hash: u32,
  prefix_hash: u32,
  is_left: bool,
  inner_len: u8,
  k_pow: u32,
  suffix_iter: Option<FixedLenSolver>,
}

impl Iterator for Solver {
  type Item = ClearVec;

  fn next(&mut self) -> Option<ClearVec> {
    loop {
      let mut suffix_solver: FixedLenSolver = match self.suffix_iter.take() {
        None => {
          // No pending suffix iterator, initialize the next one
          if self.inner_len > self.options.max_len {
            return None;
          }

          let suffix_hash = if self.is_left {
            self.hash.wrapping_sub(self.prefix_hash.wrapping_mul(self.k_pow)) & 0x7fffffff
          } else {
            self.prefix_hash.wrapping_mul(self.k_pow).wrapping_sub(self.hash) & 0x7fffffff
          };
          let inner_hash = get_suffix_search_hash(suffix_hash, self.options.suffix);
          branch_solve_with_length(inner_hash, self.inner_len)
        },
        Some(suffix_solver) => suffix_solver,
      };
      let next_suffix = suffix_solver.next();
      self.suffix_iter = Some(suffix_solver);
      match next_suffix {
        None => {
          // We exhausted the current suffix iterator, prepare the parameters for the next one
          self.inner_len += 1;
          self.k_pow = self.k_pow.wrapping_mul(u32::from(K));
          self.suffix_iter = None;
        },
        Some(cv) => {
          let full: ClearVec = self.options.prefix.concat(cv).concat(self.options.suffix);
          return Some(full);
        }
      }
    }
  }
}

fn branch_solve(hash: u32, options: SolveOptions, is_left: bool) -> impl Iterator<Item=ClearVec> {
  let prefix: ClearVec = options.prefix;
  let mut prefix_hash: u32 = 0;
  for i in (0..usize::from(prefix.len)).rev() {
    prefix_hash = prefix_hash.wrapping_mul(u32::from(K)).wrapping_add(u32::from(prefix.bytes[i]));
  }
  let prefix_hash = prefix_hash & 0x7fffffff;
  let prefix_hash = if is_left {
    prefix_hash
  } else {
    if prefix_hash == 0 { 0x40000000 } else { 0x80000000 - prefix_hash }
  };
  let mut k_pow: u32 = 1;
  for _ in 0..options.suffix.len {
    k_pow = k_pow.wrapping_mul(u32::from(K));
  }

  Solver {
    options,
    hash,
    prefix_hash,
    is_left,
    inner_len: 0,
    k_pow,
    suffix_iter: None,
  }
}

fn reverse_abs31(hash: u32) -> (u32, u32) {
  let left = hash & 0x3fffffff;
  let right = if left == 0 { 0x40000000 } else { 0x80000000 - left };
  (left, right)
}

#[allow(unused)]
pub fn solve_all(hash: u32) -> impl Iterator<Item=ClearVec> {
  let (left_hash, right_hash) = reverse_abs31(hash);
  let left = branch_solve_all(left_hash);
  let right = branch_solve_all(right_hash);
  itertools::interleave(left, right)
}

fn branch_solve_all(hash: u32) -> AllSolver {
  let first = get_base_solution_high(hash);
  AllSolver { base: first, next: first }
}

enum SuffixSolver {
  All(AllSolver),
  FixedLen(FixedLenSolver),
}

impl Iterator for SuffixSolver {
  type Item = ClearVec;

  fn next(&mut self) -> Option<ClearVec> {
    match self {
      SuffixSolver::All(ref mut solver) => solver.next(),
      SuffixSolver::FixedLen(ref mut solver) => solver.next(),
    }
  }
}

struct AllSolver {
  pub base: ClearVec,
  pub next: ClearVec,
}

impl Iterator for AllSolver {
  type Item = ClearVec;

  fn next(&mut self) -> Option<ClearVec> {
    let result = Some(self.next);
    match self.next.next_swap_be() {
      Some(next) => {
        self.next = next;
      },
      None => {
        let next_base = add_pow2_31_high(self.base);
        self.base = next_base;
        self.next = next_base;
      }
    }
    result
  }
}

pub fn solve_with_suffix(hash: u32, suffix: ClearVec, total_len: Option<u8>) -> impl Iterator<Item=ClearVec> {
  let (left_hash, right_hash) = reverse_abs31(hash);
  let left = branch_solve_with_suffix(left_hash, suffix, total_len);
  let right = branch_solve_with_suffix(right_hash, suffix, total_len);
  itertools::interleave(left, right)
}

fn get_suffix_search_hash(full_hash: u32, suffix: ClearVec) -> u32 {
  let mut hash32: u32 = 0;
  for i in (0..usize::from(suffix.len)).rev() {
    hash32 = hash32.wrapping_mul(u32::from(K)).wrapping_add(u32::from(suffix.bytes[i]));
  }

  let mut inv_k_pow: u32 = 1;
  for _ in 0..suffix.len {
    inv_k_pow = inv_k_pow.wrapping_mul(K_INV);
  }

  full_hash.wrapping_sub(hash32).wrapping_mul(inv_k_pow) & 0x7fffffff
}

fn branch_solve_with_suffix(hash: u32, suffix: ClearVec, total_len: Option<u8>) -> impl Iterator<Item=ClearVec> {
  let search_hash = get_suffix_search_hash(hash, suffix);

  let solver = match total_len {
    Some(len) => {
      SuffixSolver::FixedLen(branch_solve_with_length(search_hash, len.saturating_sub(suffix.len)))
    },
    None => {
      SuffixSolver::All(branch_solve_all(search_hash))
    }
  };

  solver.map(move |prefix| prefix.concat(suffix))
}

struct PrefixSolver {
  pub hash: u32,
  pub prefix_hash: u32,
  pub is_left: bool,
  pub k_pow: u32,
  pub suffix_iter: Option<FixedLenSolver>,
}

impl Iterator for PrefixSolver {
  type Item = ClearVec;

  fn next(&mut self) -> Option<ClearVec> {
    let next:ClearVec = match &mut self.suffix_iter {
      None => {
        let mut len = 0;
        loop {
          let mut suffix_iter = if self.is_left {
            let shifted_hash = self.prefix_hash.wrapping_mul(self.k_pow);
            let hash_diff = self.hash.wrapping_sub(shifted_hash) & 0x7fffffff;
            branch_solve_with_length(hash_diff, len)
          } else {
            let shifted_hash = self.prefix_hash.wrapping_mul(self.k_pow);
            let hash_diff = shifted_hash.wrapping_sub(self.hash) & 0x7fffffff;
            branch_solve_with_length(hash_diff, len)
          };
          match suffix_iter.next() {
            None => {},
            Some(cv) => {
              self.suffix_iter = Some(suffix_iter);
              break cv
            }
          }
          len += 1;
          self.k_pow = self.k_pow.wrapping_mul(u32::from(K));
        }
      },
      Some(suffix_iter) => {
        let next_in_iter = suffix_iter.next();
        let next_cv = match next_in_iter {
          Some(next_cv) => next_cv,
          None => {
            let mut len = suffix_iter.len;
            loop {
              len += 1;
              self.k_pow = self.k_pow.wrapping_mul(u32::from(K));
              let mut suffix_iter = if self.is_left {
                let shifted_hash = self.prefix_hash.wrapping_mul(self.k_pow);
                let hash_diff = self.hash.wrapping_sub(shifted_hash) & 0x7fffffff;
                branch_solve_with_length(hash_diff, len)
              } else {
                let shifted_hash = self.prefix_hash.wrapping_mul(self.k_pow);
                let hash_diff = shifted_hash.wrapping_sub(self.hash) & 0x7fffffff;
                branch_solve_with_length(hash_diff, len)
              };
              match suffix_iter.next() {
                None => {},
                Some(cv) => {
                  self.suffix_iter = Some(suffix_iter);
                  break cv
                }
              }
            }
          }
        };
        next_cv
      }
    };
    Some(next)
  }
}

pub fn solve_with_prefix(hash: u32, prefix: ClearVec) -> impl Iterator<Item=ClearVec> {
  let left = branch_solve_with_prefix(hash, prefix, true);
  let right = branch_solve_with_prefix(hash, prefix, false);
  itertools::interleave(left, right)
}

fn branch_solve_with_prefix(hash: u32, prefix: ClearVec, is_left: bool) -> impl Iterator<Item=ClearVec> {
  let mut prefix_hash: u32 = 0;
  for i in (0..usize::from(prefix.len)).rev() {
    prefix_hash = prefix_hash.wrapping_mul(u32::from(K)).wrapping_add(u32::from(prefix.bytes[i]));
  }
  let prefix_hash = prefix_hash & 0x7fffffff;
  let prefix_hash = if is_left {
    prefix_hash
  } else {
    if prefix_hash == 0 { 0x40000000 } else { 0x80000000 - prefix_hash }
  };

  let solver = PrefixSolver {
    hash,
    prefix_hash,
    is_left,
    k_pow: 1,
    suffix_iter: None,
  };

  solver.map(move |suffix| prefix.concat(suffix))
}

#[allow(unused)]
pub fn solve_with_length(hash: u32, len: u8) -> impl Iterator<Item=ClearVec> {
  let (left_hash, right_hash) = reverse_abs31(hash);
  let left = branch_solve_with_length(left_hash, len);
  let right = branch_solve_with_length(right_hash, len);
  itertools::interleave(left, right)
}

fn branch_solve_with_length(hash: u32, len: u8) -> FixedLenSolver {
  let pow2_31_base = get_pow2_31_lte_len_low(len);
  let hash_base = get_base_solution_low(hash);

  let root_low = add_low(pow2_31_base, hash_base);
  let root_low = if root_low.len < len {
    add_low(root_low, ClearVec::from_slice_le(&POW2_31_LOW))
  } else {
    root_low
  };
  let root_high = root_low.high_repr();

  if root_high.len > len {
    // The length of the high root is the shortest we can ever get, if `len` is even smaller then
    // there are no solutions at all
    return FixedLenSolver { base: root_high, len, next: None };
  }

  // Find the shortest CV that is long enough
  let first: ClearVec = if root_high.len == len {
    // root_high (the shortest) is already long enough we can skip probbing
    root_high
  } else {
    // The root falls in a "transition interval" where `len` is on
    // the longer side (low coefficients). Iter backward to find the first CV with the expected
    // length.
    let mut first = root_low;
    while let Some(prev) = first.prev_swap_be() {
      if prev.len >= len {
        first = prev;
      } else {
        break;
      }
    }
    first
  };

  debug_assert_eq!(first.len, len);

  return FixedLenSolver { base: root_high, len, next: Some(first) };
}

/// Returns a ClearVec multiple of 2**31 lesser than or equal to the cutoff value of `len`
///
/// Returns the largest `a` such that `a % 2**31 == 0` and `a <= 51**(len-1)`,
/// using high representation.
///
/// | length  | 51**(len-1)        | result        |
/// |---------|--------------------|---------------|
/// | (-inf)0 |                  0 |             0 |
/// |       1 |           1 = 2**0 |             0 |
/// |       2 |   51 ≃ 1.59 * 2**5 |             0 |
/// |       3 | 51² ≃ 1.27 * 2**11 |             0 |
/// |       4 | 51³ ≃ 1.01 * 2**17 |             0 |
/// |       5 | 51⁴ ≃ 1.61 * 2**22 |             0 |
/// |       6 | 51⁵ ≃ 1.61 * 2**28 |             0 |
/// |       7 | 51⁶ ≃ 1.02 * 2**34 |     8 * 2**31 |
/// |       8 | 51⁷ ≃ 1.63 * 2**39 |   417 * 2**31 |
/// |       9 | 51⁸ ≃ 1.30 * 2**45 | 21312 * 2**31 |
/// |      10 | 51⁸ ≃ 1.30 * 2**45 | 21312 * 2**31 |
fn get_pow2_31_lte_len_low(len: u8) -> ClearVec {
  let pow2_31_table = get_pow2_31_low_table();

  let mut best = ClearVec::from_slice_le(&[]);
  for pow2_31 in pow2_31_table.iter().rev() {
    if pow2_31.len > len {
      // Slight optimization: avoid addition if we know that the length will be to long.
      continue;
    }
    let next = add_low(best, *pow2_31);
    if next.len < len {
      // A strict inequality on the length is sufficient to detect `a < 51**(len-1)`
      // Since there are no `a` such that `a == 51**(len-1)`, this ensures correctness.
      best = next;
    }
  }

  best
}

/// Returns a ClearVec multiple of 2**31 lesser than or equal to the cutoff value of `len`
///
/// Computes `2**(31+i)` for i in `[0, 50[`
///
/// |  i | value |
/// |----|-------|
/// |  0 |   2³¹ |
/// |  1 |   2³² |
/// |  2 |   2³³ |
/// The table is computed to contain the power `floor((CAPACITY + 1) * log2 (51)) == 181`
fn get_pow2_31_low_table() -> Vec<ClearVec> {
  let mut cur: ClearVec = ClearVec::from_slice_le(&POW2_31_LOW);
  let max = ((CAPACITY as f64 + 1f64) * (K as f64).log2()).floor() as usize;
  let mut table: Vec<ClearVec> = vec![cur];
  for _ in 32..=max {
    cur = add_low(cur, cur);
    table.push(cur);
  }
  table
}

struct FixedLenSolver {
  pub base: ClearVec,
  pub len: u8,
  pub next: Option<ClearVec>,
}

impl Iterator for FixedLenSolver {
  type Item = ClearVec;

  fn next(&mut self) -> Option<ClearVec> {
    match self.next {
      Some(cv) => {
        let next_swap = cv.next_swap_be()
          .filter(|cv| cv.len == self.len);

        match next_swap {
          Some(next_swap) => {
            self.next = Some(next_swap)
          }
          None => {
            // We exhausted the current base solution and need to jump to the next one
            let base_high = add_pow2_31_high(self.base);

            if base_high.len > self.len {
              // The length of the high root is the shortest we can ever get, if `len` is even smaller then
              // there are no solutions at all
              self.next = None;
            } else {
              // Find the shortest CV that is long enough
              let first: ClearVec = if base_high.len == self.len {
                // root_high (the shortest) is already long enough we can skip probbing
                base_high
              } else {
                // The root falls in a "transition interval" where `len` is on
                // the longer side (low coefficients). Iter backward to find the first CV with the expected
                // length.
                let mut first = base_high.low_repr();
                while let Some(prev) = first.prev_swap_be() {
                  if prev.len >= self.len {
                    first = prev;
                  } else {
                    break;
                  }
                }
                first
              };
              debug_assert_eq!(first.len, self.len);
              self.base = base_high;
              self.next = Some(first);
            }
          }
        }
        debug_assert_eq!(cv.len, self.len);
        debug_assert_ne!(self.next, Some(cv));
        Some(cv)
      }
      None => None
    }
  }
}

/// Returns a clear vec with all values in `[0, K[`
fn get_base_solution_low(mut hash: u32) -> ClearVec {
  let mut bytes: [u8; CAPACITY] = [0u8; CAPACITY];
  let mut len: usize = 0;
  while hash > 0 && len < CAPACITY {
    let next_hash = hash / u32::from(K);
    let digit = hash % u32::from(K);
    bytes[len] = u8::try_from(digit).unwrap_or(0);
    hash = next_hash;
    len += 1;
  }
  ClearVec { len: len.try_into().unwrap(), bytes }
}

/// Returns a clear vec with all values in `[N-K, N[`, except the big end in `[0, N[`
fn get_base_solution_high(mut hash: u32) -> ClearVec {
  let mut bytes: [u8; CAPACITY] = [0u8; CAPACITY];
  let mut len: usize = 0;
  while hash > 0 && len < CAPACITY {
    let mut next_hash = hash / u32::from(K);
    let mut digit = hash % u32::from(K);
    if digit < u32::from(N - K) && next_hash >= 1 {
      digit += u32::from(K);
      next_hash -= 1;
    }
    bytes[len] = u8::try_from(digit).unwrap_or(0);
    hash = next_hash;
    len += 1;
  }
  ClearVec { len: len.try_into().unwrap(), bytes }
}

fn add_pow2_31_high(cv: ClearVec) -> ClearVec {
  add_high(cv, ClearVec::from_slice_le(&POW2_31_HIGH))
}

fn add_high(left: ClearVec, right: ClearVec) -> ClearVec {
  let mut bytes: [u8; CAPACITY] = left.bytes;
  let max_len: u8 = std::cmp::max(left.len, right.len);
  let mut carry: u8 = 0;
  for i in 0..max_len {
    if i < right.len {
      bytes[usize::from(i)] += right.bytes[usize::from(i)];
    }
    bytes[usize::from(i)] += carry;
    carry = 0;
    while bytes[usize::from(i)] >= N {
      bytes[usize::from(i)] -= K;
      carry += 1;
    }
  }
  let len: u8 = if carry == 0 {
    max_len
  } else {
    bytes[usize::from(max_len)] = carry;
    max_len + 1
  };
  ClearVec { len, bytes }
}

fn add_low(left: ClearVec, right: ClearVec) -> ClearVec {
  let mut bytes: [u8; CAPACITY] = left.bytes;
  let max_len: u8 = std::cmp::max(left.len, right.len);
  let mut carry: u8 = 0;
  for i in 0..max_len {
    if i < right.len {
      bytes[usize::from(i)] += right.bytes[usize::from(i)];
    }
    bytes[usize::from(i)] += carry;
    carry = 0;
    if bytes[usize::from(i)] >= K {
      bytes[usize::from(i)] -= K;
      carry += 1;
    }
  }
  let len: u8 = if carry == 0 || usize::from(max_len) == CAPACITY {
    max_len
  } else {
    bytes[usize::from(max_len)] = carry;
    max_len + 1
  };
  ClearVec { len, bytes }
}

pub trait HashSwap: Sized {
  fn next_swap_be(self) -> Option<Self>;
  fn prev_swap_be(self) -> Option<Self>;
  fn next_swap_le(self) -> Option<Self>;
}

impl HashSwap for ClearVec {
  fn next_swap_be(self) -> Option<Self> {
    let pivot: usize = next_swap_be_find(self);
    if pivot < CAPACITY {
      Some(next_swap_be_apply(self, pivot))
    } else {
      None
    }
  }

  fn prev_swap_be(self) -> Option<Self> {
    let pivot: usize = prev_swap_be_find(self);
    if pivot < CAPACITY {
      Some(prev_swap_be_apply(self, pivot))
    } else {
      None
    }
  }

  fn next_swap_le(self) -> Option<Self> {
    let pivot: usize = next_swap_le_find(self);
    NonZeroUsize::new(pivot).map(|x| next_swap_le_apply(self, x))
  }
}

pub trait HashRepr: Sized {
  fn low_repr(self) -> Self;
  fn high_repr(self) -> Self;
}

impl HashRepr for ClearVec {
  fn low_repr(mut self) -> Self {
    for i in 0..usize::from(self.len) {
      if self.bytes[i] >= K && usize::from(i + 1) < CAPACITY {
        self.bytes[i] -= K;
        self.bytes[i + 1] += 1;
      }
    }
    if self.bytes[usize::from(self.len)] != 0 {
      self.len += 1;
    }
    self
  }

  fn high_repr(mut self) -> Self {
    // This algorithm works because the last digit is guaranteed to be non-zero.
    for i in 0..usize::from(self.len.saturating_sub(1)) {
      if self.bytes[i] < (N - K) || self.bytes[i] == std::u8::MAX {
        self.bytes[i] = self.bytes[i].wrapping_add(K);
        self.bytes[i + 1] = self.bytes[i + 1].wrapping_sub(1);
      }
    }
    if self.len > 0 && self.bytes[usize::from(self.len - 1)] == 0 {
      self.len -= 1;
    }
    self
  }
}

/// Finds the index of the LE pivot, or returns 0 if there are none
///
/// Find the largest i such that you can do:
/// bytes[i-1] += K
/// bytes[i  ] -= 1
fn next_swap_le_find(cv: ClearVec) -> usize {
  if cv.len == 0 {
    return 0;
  }
  for i in (1..usize::from(cv.len)).rev() {
    if cv.bytes[i - 1] < (N - K) && cv.bytes[i] >= 1 {
      return i;
    }
  }
  return 0;
}

/// Apply pivot and normalize right side
fn next_swap_le_apply(mut cv: ClearVec, pivot_index: NonZeroUsize) -> ClearVec {
  cv.bytes[usize::from(pivot_index) - 1] += K;
  cv.bytes[usize::from(pivot_index)] -= 1;
  let pivot_is_last = usize::from(pivot_index) == usize::from(cv.len - 1);
  if pivot_is_last && cv.bytes[usize::from(pivot_index)] == 0 {
    cv.len -= 1;
  }

  for i in usize::from(pivot_index)..usize::from(cv.len) {
    if cv.bytes[i] >= K && usize::from(i + 1) < CAPACITY {
      cv.bytes[i] -= K;
      cv.bytes[i + 1] += 1;
    }
  }
  if cv.bytes[usize::from(cv.len)] != 0 {
    cv.len += 1;
  }
  cv
}

/// Finds the index of the BE pivot, or returns `CAPACITY` if there are none
///
/// Find the lowest i such that you can do:
/// bytes[i] += K
/// bytes[i+1]  -= 1
fn prev_swap_be_find(cv: ClearVec) -> usize {
  if cv.len == 0 {
    return CAPACITY;
  }
  for i in 0..usize::from(cv.len - 1) {
    if cv.bytes[i] < (N - K) && cv.bytes[i + 1] >= 1 {
      return i;
    }
  }
  return CAPACITY;
}

fn prev_swap_be_apply(mut cv: ClearVec, pivot_index: usize) -> ClearVec {
  cv.bytes[pivot_index] += K;
  cv.bytes[pivot_index + 1] -= 1;
  if pivot_index + 1 == usize::from(cv.len - 1) && cv.bytes[pivot_index + 1] == 0 {
    cv.len -= 1;
  }

  let mut non_max_before_pivot = pivot_index;
  while non_max_before_pivot > 0 {
    if cv.bytes[non_max_before_pivot] != (N - 1) {
      break;
    } else {
      non_max_before_pivot -= 1
    }
  }

  for i in 0..non_max_before_pivot {
    if cv.bytes[i] >= K {
      cv.bytes[i] -= K;
      cv.bytes[i + 1] += 1;
    }
  }

  cv
}

/// Finds the index of the BE pivot, or returns `CAPACITY` if there are none
///
/// Find the lowest i such that you can do:
/// bytes[i] -= K
/// bytes[i+1] += 1
fn next_swap_be_find(cv: ClearVec) -> usize {
  if cv.len == 0 || usize::from(cv.len) == CAPACITY {
    return CAPACITY;
  }
  for i in 0..usize::from(cv.len) {
    if cv.bytes[i] >= K && (cv.bytes[i + 1] + 1) < N {
      return i;
    }
  }
  return CAPACITY;
}

fn next_swap_be_apply(mut cv: ClearVec, pivot_index: usize) -> ClearVec {
  cv.bytes[pivot_index] -= K;
  cv.bytes[pivot_index + 1] += 1;
  if pivot_index + 1 == usize::from(cv.len) {
    cv.len += 1;
  }

  let mut non_zero_before_pivot = pivot_index;
  while non_zero_before_pivot > 0 {
    if cv.bytes[non_zero_before_pivot] != 0 {
      break;
    } else {
      non_zero_before_pivot -= 1
    }
  }

  for i in 0..non_zero_before_pivot {
    if cv.bytes[i] < (N - K) || cv.bytes[i] == std::u8::MAX {
      cv.bytes[i] = cv.bytes[i].wrapping_add(K);
      cv.bytes[i + 1] = cv.bytes[i + 1].wrapping_sub(1);
    }
  }

  cv
}


// * Adds 2 ** 31 and normalizes to base 51
// */
//#[allow(dead_code)]
//fn next_seed(mut digits: Vec<u8>) -> Vec<u8> {
//  while digits.len() < POW31_B51.len() {
//    digits.push(0u8);
//  }
//  let mut carry: u8 = 0;
//  for i in 0..digits.len() {
//    let cur: u8 = digits[i] + carry + POW31_B51.get(i).unwrap_or(&0u8);
//    carry = cur / K;
//    digits[i] = cur % K;
//  }
//  if carry != 0 {
//    // assert: carry < K
//    digits.push(carry);
//  }
//  digits
//}

#[cfg(test)]
mod tests {
  use super::*;
  use std::collections::HashSet;

//  #[test]
//  fn test_next_seed() {
//    let test_items: Vec<(Vec<u8>, Vec<u8>)> = vec![
//      (vec![], vec![26u8, 35u8, 49u8, 21u8, 11u8, 6u8]),
//      (vec![1u8], vec![27u8, 35u8, 49u8, 21u8, 11u8, 6u8]),
//    ];
//    for (input, expected) in test_items.into_iter() {
//      assert_eq!(next_seed(input), expected);
//    }
//  }

  #[test]
  fn test_get_base_solution_low() {
    let test_items: Vec<(u32, ClearVec)> = vec![
      (1, ClearVec::from_slice_le(&[1])),
      (31836, ClearVec::from_slice_le(&[12, 12, 12])),
      (8489727, ClearVec::from_slice_le(&[12, 01, 00, 13, 01])),
    ];
    for (input, expected) in test_items.into_iter() {
      assert_eq!(get_base_solution_low(input), expected);
    }
  }

  #[test]
  fn test_get_base_solution_high() {
    let test_items: Vec<(u32, ClearVec)> = vec![
      (1, ClearVec::from_slice_le(&[1])),
      (31836, ClearVec::from_slice_le(&[63, 62, 11])),
      (8489727, ClearVec::from_slice_le(&[63, 51, 50, 63])),
    ];
    for (input, expected) in test_items.into_iter() {
      assert_eq!(get_base_solution_high(input), expected);
    }
  }

  #[test]
  fn test_next_swap_le() {
    let test_items: Vec<(ClearVec, Option<ClearVec>)> = vec![
      (ClearVec::from_slice_le(&[00, 01, 01]), Some(ClearVec::from_slice_le(&[00, 52]))),
      (ClearVec::from_slice_le(&[00, 52]), Some(ClearVec::from_slice_le(&[51, 00, 01]))),
      (ClearVec::from_slice_le(&[51, 00, 01]), Some(ClearVec::from_slice_le(&[51, 51]))),
      (ClearVec::from_slice_le(&[51, 51]), None),
      (ClearVec::from_slice_le(&[12, 12, 12]), Some(ClearVec::from_slice_le(&[12, 63, 11]))),
      (ClearVec::from_slice_le(&[12, 63, 11]), Some(ClearVec::from_slice_le(&[63, 11, 12]))),
      (ClearVec::from_slice_le(&[63, 11, 12]), Some(ClearVec::from_slice_le(&[63, 62, 11]))),
      (ClearVec::from_slice_le(&[63, 62, 11]), None),
      (ClearVec::from_slice_le(&[12, 13, 12]), Some(ClearVec::from_slice_le(&[63, 12, 12]))),
      (ClearVec::from_slice_le(&[63, 12, 12]), Some(ClearVec::from_slice_le(&[63, 63, 11]))),
      (ClearVec::from_slice_le(&[63, 63, 11]), None),
      (ClearVec::from_slice_le(&[12, 01, 00, 13, 01]), Some(ClearVec::from_slice_le(&[12, 01, 51, 12, 01]))),
      (ClearVec::from_slice_le(&[12, 01, 51, 12, 01]), Some(ClearVec::from_slice_le(&[12, 01, 51, 63]))),
      (ClearVec::from_slice_le(&[12, 01, 51, 63]), Some(ClearVec::from_slice_le(&[12, 52, 50, 12, 01]))),
      (ClearVec::from_slice_le(&[12, 52, 50, 12, 01]), Some(ClearVec::from_slice_le(&[12, 52, 50, 63]))),
      (ClearVec::from_slice_le(&[12, 52, 50, 63]), Some(ClearVec::from_slice_le(&[63, 00, 00, 13, 01]))),
      (ClearVec::from_slice_le(&[63, 00, 00, 13, 01]), Some(ClearVec::from_slice_le(&[63, 00, 51, 12, 1]))),
      (ClearVec::from_slice_le(&[63, 00, 51, 12, 01]), Some(ClearVec::from_slice_le(&[63, 00, 51, 63]))),
      (ClearVec::from_slice_le(&[63, 00, 51, 63]), Some(ClearVec::from_slice_le(&[63, 51, 50, 12, 01]))),
      (ClearVec::from_slice_le(&[63, 51, 50, 12, 01]), Some(ClearVec::from_slice_le(&[63, 51, 50, 63]))),
      (ClearVec::from_slice_le(&[63, 51, 50, 63]), None),
    ];
    for (input, expected) in test_items.into_iter() {
      let actual = input.next_swap_le();
      assert_eq!(actual, expected);
    }
  }

  #[test]
  fn test_prev_swap_be() {
    let test_items: Vec<(ClearVec, Option<ClearVec>)> = vec![
      (ClearVec::from_slice_le(&[00, 01, 01]), Some(ClearVec::from_slice_le(&[51, 00, 01]))),
      (ClearVec::from_slice_le(&[51, 00, 01]), Some(ClearVec::from_slice_le(&[00, 52]))),
      (ClearVec::from_slice_le(&[00, 52]), Some(ClearVec::from_slice_le(&[51, 51]))),
      (ClearVec::from_slice_le(&[51, 51]), None),
      (ClearVec::from_slice_le(&[12, 12, 12]), Some(ClearVec::from_slice_le(&[63, 11, 12]))),
      (ClearVec::from_slice_le(&[63, 11, 12]), Some(ClearVec::from_slice_le(&[12, 63, 11]))),
      (ClearVec::from_slice_le(&[12, 63, 11]), Some(ClearVec::from_slice_le(&[63, 62, 11]))),
      (ClearVec::from_slice_le(&[63, 62, 11]), None),
      (ClearVec::from_slice_le(&[12, 13, 12]), Some(ClearVec::from_slice_le(&[63, 12, 12]))),
      (ClearVec::from_slice_le(&[63, 12, 12]), Some(ClearVec::from_slice_le(&[63, 63, 11]))),
      (ClearVec::from_slice_le(&[63, 63, 11]), None),
      (ClearVec::from_slice_le(&[12, 01, 00, 13, 01]), Some(ClearVec::from_slice_le(&[63, 00, 00, 13, 01]))),
      (ClearVec::from_slice_le(&[63, 00, 00, 13, 01]), Some(ClearVec::from_slice_le(&[12, 01, 51, 12, 01]))),
      (ClearVec::from_slice_le(&[12, 01, 51, 12, 01]), Some(ClearVec::from_slice_le(&[63, 00, 51, 12, 01]))),
      (ClearVec::from_slice_le(&[63, 00, 51, 12, 01]), Some(ClearVec::from_slice_le(&[12, 52, 50, 12, 01]))),
      (ClearVec::from_slice_le(&[12, 52, 50, 12, 01]), Some(ClearVec::from_slice_le(&[63, 51, 50, 12, 01]))),
      (ClearVec::from_slice_le(&[63, 51, 50, 12, 01]), Some(ClearVec::from_slice_le(&[12, 01, 51, 63]))),
      (ClearVec::from_slice_le(&[12, 01, 51, 63]), Some(ClearVec::from_slice_le(&[63, 00, 51, 63]))),
      (ClearVec::from_slice_le(&[63, 00, 51, 63]), Some(ClearVec::from_slice_le(&[12, 52, 50, 63]))),
      (ClearVec::from_slice_le(&[12, 52, 50, 63]), Some(ClearVec::from_slice_le(&[63, 51, 50, 63]))),
      (ClearVec::from_slice_le(&[63, 51, 50, 63]), None),
      (ClearVec::from_slice_le(&[61, 12, 54, 8, 6]), Some(ClearVec::from_slice_le(&[61, 63, 53, 8, 6]))),
      (ClearVec::from_slice_le(&[60, 50, 30, 5, 5]), Some(ClearVec::from_slice_le(&[9, 0, 31, 56, 4]))),
      (ClearVec::from_slice_le(&[9, 0, 31, 56, 4]), Some(ClearVec::from_slice_le(&[9, 51, 30, 56, 4]))),
      (ClearVec::from_slice_le(&[9, 51, 30, 5, 5]), Some(ClearVec::from_slice_le(&[60, 50, 30, 5, 5]))),
      (ClearVec::from_slice_le(&[60, 50, 30, 5, 5]), Some(ClearVec::from_slice_le(&[9, 0, 31, 56, 4]))),
    ];
    for (input, expected) in test_items.into_iter() {
      let actual = input.prev_swap_be();
      assert_eq!(actual, expected);
    }
  }

  #[test]
  fn test_next_swap_be() {
    let act = ClearVec::from_slice_le(&[0, 60, 42, 29, 17, 37]).next_swap_be();

    let test_items: Vec<(ClearVec, Option<ClearVec>)> = vec![
      (ClearVec::from_slice_le(&[51, 51]), Some(ClearVec::from_slice_le(&[00, 52]))),
      (ClearVec::from_slice_le(&[00, 52]), Some(ClearVec::from_slice_le(&[51, 00, 01]))),
      (ClearVec::from_slice_le(&[51, 00, 01]), Some(ClearVec::from_slice_le(&[00, 01, 01]))),
      (ClearVec::from_slice_le(&[00, 01, 01]), None),
      (ClearVec::from_slice_le(&[63, 62, 11]), Some(ClearVec::from_slice_le(&[12, 63, 11]))),
      (ClearVec::from_slice_le(&[12, 63, 11]), Some(ClearVec::from_slice_le(&[63, 11, 12]))),
      (ClearVec::from_slice_le(&[63, 11, 12]), Some(ClearVec::from_slice_le(&[12, 12, 12]))),
      (ClearVec::from_slice_le(&[12, 12, 12]), None),
      (ClearVec::from_slice_le(&[63, 63, 11]), Some(ClearVec::from_slice_le(&[63, 12, 12]))),
      (ClearVec::from_slice_le(&[63, 12, 12]), Some(ClearVec::from_slice_le(&[12, 13, 12]))),
      (ClearVec::from_slice_le(&[12, 13, 12]), None),
      (ClearVec::from_slice_le(&[63, 51, 50, 63]), Some(ClearVec::from_slice_le(&[12, 52, 50, 63]))),
      (ClearVec::from_slice_le(&[12, 52, 50, 63]), Some(ClearVec::from_slice_le(&[63, 00, 51, 63]))),
      (ClearVec::from_slice_le(&[63, 00, 51, 63]), Some(ClearVec::from_slice_le(&[12, 01, 51, 63]))),
      (ClearVec::from_slice_le(&[12, 01, 51, 63]), Some(ClearVec::from_slice_le(&[63, 51, 50, 12, 01]))),
      (ClearVec::from_slice_le(&[63, 51, 50, 12, 01]), Some(ClearVec::from_slice_le(&[12, 52, 50, 12, 01]))),
      (ClearVec::from_slice_le(&[12, 52, 50, 12, 01]), Some(ClearVec::from_slice_le(&[63, 00, 51, 12, 01]))),
      (ClearVec::from_slice_le(&[63, 00, 51, 12, 01]), Some(ClearVec::from_slice_le(&[12, 01, 51, 12, 01]))),
      (ClearVec::from_slice_le(&[12, 01, 51, 12, 01]), Some(ClearVec::from_slice_le(&[63, 00, 00, 13, 01]))),
      (ClearVec::from_slice_le(&[63, 00, 00, 13, 01]), Some(ClearVec::from_slice_le(&[12, 01, 00, 13, 01]))),
      (ClearVec::from_slice_le(&[12, 01, 00, 13, 01]), None),
      (ClearVec::from_slice_le(&[51, 59, 42, 29, 17, 37]), Some(ClearVec::from_slice_le(&[0, 60, 42, 29, 17, 37]))),
      (ClearVec::from_slice_le(&[0, 60, 42, 29, 17, 37]), Some(ClearVec::from_slice_le(&[51, 8, 43, 29, 17, 37]))),
      (ClearVec::from_slice_le(&[51, 8, 43, 29, 17, 37]), Some(ClearVec::from_slice_le(&[0, 9, 43, 29, 17, 37]))),
      (ClearVec::from_slice_le(&[0, 9, 43, 29, 17, 37]), None),
      (ClearVec::from_slice_le(&[10, 13, 3, 60, 5]), Some(ClearVec::from_slice_le(&[61, 63, 53, 8, 6]))),
      (ClearVec::from_slice_le(&[61, 63, 53, 8, 6]), Some(ClearVec::from_slice_le(&[61, 12, 54, 8, 6]))),
      (ClearVec::from_slice_le(&[9, 51, 30, 56, 4]), Some(ClearVec::from_slice_le(&[9, 0, 31, 56, 4]))),
      (ClearVec::from_slice_le(&[30, 0, 12, 01, 51, 12, 01]), Some(ClearVec::from_slice_le(&[30, 51, 62, 00, 00, 13, 01]))),
      (ClearVec::from_slice_le(&[9, 0, 31, 56, 4]), Some(ClearVec::from_slice_le(&[60, 50, 30, 5, 5]))),
      (ClearVec::from_slice_le(&[60, 50, 30, 5, 5]), Some(ClearVec::from_slice_le(&[9, 51, 30, 5, 5]))),
    ];
    for (input, expected) in test_items.into_iter() {
      let actual = input.next_swap_be();
      assert_eq!(actual, expected);
    }
  }

  #[test]
  fn test_solve_all() {
    let actual: Vec<ClearVec> = solve_all(3).collect();
    let expected = vec![ClearVec::from_slice_le(&[3])];

    assert_eq!(actual, expected);
  }

  #[test]
  fn test_solve_with_length() {
    {
      {
        let actual: Vec<ClearVec> = solve_with_length(3, 0).collect();
        let expected: Vec<ClearVec> = vec![];
        assert_eq!(actual, expected);
      }
      {
        let actual: Vec<ClearVec> = solve_with_length(3, 1).collect();
        let expected: Vec<ClearVec> = vec![ClearVec::from_slice_le(&[3])];
        assert_eq!(actual, expected);
      }
      {
        let actual: Vec<ClearVec> = solve_with_length(3, 2).collect();
        let expected: Vec<ClearVec> = vec![];
        assert_eq!(actual, expected);
      }
      {
        let actual: Vec<ClearVec> = solve_with_length(3, 3).collect();
        let expected: Vec<ClearVec> = vec![];
        assert_eq!(actual, expected);
      }
      {
        let actual: Vec<ClearVec> = solve_with_length(3, 4).collect();
        let expected: Vec<ClearVec> = vec![];
        assert_eq!(actual, expected);
      }
      {
        let actual: Vec<ClearVec> = solve_with_length(3, 5).collect();
        let expected: Vec<ClearVec> = vec![];
        assert_eq!(actual, expected);
      }
      {
        let actual: Vec<ClearVec> = solve_with_length(3, 6).collect();
        let expected: Vec<ClearVec> = vec![
          ClearVec::from_str("e9uWIC"),
          ClearVec::from_str("e9uWIw"),
          ClearVec::from_str("fkuWIC"),
          ClearVec::from_str("fkuWIw"),
          ClearVec::from_str("lvQVs2"),
          ClearVec::from_str("lvQVsW"),
          ClearVec::from_str("lvQVtd"),
          ClearVec::from_str("rHnT2x"),
          ClearVec::from_str("rHnT2D"),
          ClearVec::from_str("rHnUdx"),
          ClearVec::from_str("rHnUdD"),
          ClearVec::from_str("xSJSMX"),
          ClearVec::from_str("xSJSM3"),
          ClearVec::from_str("D35Rxy"),
          ClearVec::from_str("xSJSNe"),
          ClearVec::from_str("D4gRxy"),
          ClearVec::from_str("D35RxE"),
          ClearVec::from_str("Ee5Rxy"),
          ClearVec::from_str("D4gRxE"),
          ClearVec::from_str("EfgRxy"),
          ClearVec::from_str("Ee5RxE"),
          ClearVec::from_str("KqCP6Y"),
          ClearVec::from_str("EfgRxE"),
          ClearVec::from_str("KqCP7$"),
          ClearVec::from_str("KqCP64"),
          ClearVec::from_str("KqCQhY"),
          ClearVec::from_str("KqCP7f"),
          ClearVec::from_str("KqCQi$"),
          ClearVec::from_str("KqCQh4"),
          ClearVec::from_str("QBYORz"),
          ClearVec::from_str("KqCQif"),
          ClearVec::from_str("QC$ORz"),
          ClearVec::from_str("QBYORF"),
          ClearVec::from_str("WNvNBZ"),
          ClearVec::from_str("QC$ORF"),
          ClearVec::from_str("WNvNCa"),
          ClearVec::from_str("WNvNB5"),
          ClearVec::from_str("2YRMmA"),
          ClearVec::from_str("WNvNCg"),
          ClearVec::from_str("3$RMmA"),
          ClearVec::from_str("2YRMmG"),
          ClearVec::from_str("8_oKV0"),
          ClearVec::from_str("3$RMmG"),
          ClearVec::from_str("8_oKWb"),
          ClearVec::from_str("8_oKV6"),
          ClearVec::from_str("9loKV0"),
          ClearVec::from_str("8_oKWh"),
          ClearVec::from_str("9loKWb"),
          ClearVec::from_str("9loKV6"),
          ClearVec::from_str("9loKWh"),
        ];
        assert_eq!(actual, expected);
      }
    }

    {
      {
        let actual: Vec<ClearVec> = branch_solve_with_length(2652, 1).collect();
        let expected: Vec<ClearVec> = vec![];
        assert_eq!(actual, expected);
      }
      {
        let actual: Vec<ClearVec> = branch_solve_with_length(2652, 2).collect();
        let expected: Vec<ClearVec> = vec![
          ClearVec::from_slice_le(&[51, 51]),
          ClearVec::from_slice_le(&[00, 52]),
        ];
        assert_eq!(actual, expected);
      }
      {
        let actual: Vec<ClearVec> = branch_solve_with_length(2652, 3).collect();
        let expected: Vec<ClearVec> = vec![
          ClearVec::from_slice_le(&[51, 00, 01]),
          ClearVec::from_slice_le(&[00, 01, 01]),
        ];
        assert_eq!(actual, expected);
      }
      {
        let actual: Vec<ClearVec> = branch_solve_with_length(2652, 4).collect();
        let expected: Vec<ClearVec> = vec![];
        assert_eq!(actual, expected);
      }
    }
  }

  #[test]
  fn test_branch_solve_with_length() {
    let actual: Vec<ClearVec> = branch_solve_with_length(595907725, 7).take(4).collect();
    let expected: Vec<ClearVec> = vec![
      ClearVec::from_str("a$zz2Kn"),
      ClearVec::from_str("a$zAdKn"),
      ClearVec::from_str("afKV1uN"),
      ClearVec::from_str("afKWcuN"),
    ];
    assert_eq!(actual, expected);
  }

  #[test]
  #[ignore]
  fn test_swaps_be() {
    for x0 in 1..N {
      let hash: u32 = u32::from(x0);
      check(hash, ClearVec::from_slice_le(&[x0]));
    }
    for x0 in 0..N {
      for x1 in 1..N {
        let hash: u32 = u32::from(x1);
        let hash: u32 = hash * 51 + u32::from(x0);
        check(hash, ClearVec::from_slice_le(&[x0, x1]));
      }
    }
    for x0 in 0..N {
      for x1 in 0..N {
        for x2 in 1..N {
          let hash: u32 = u32::from(x2);
          let hash: u32 = hash * 51 + u32::from(x1);
          let hash: u32 = hash * 51 + u32::from(x0);
          check(hash, ClearVec::from_slice_le(&[x0, x1, x2]));
        }
      }
    }
    for x0 in 0..N {
      for x1 in 0..N {
        for x2 in 0..N {
          for x3 in 1..N {
            let hash: u32 = u32::from(x3);
            let hash: u32 = hash * 51 + u32::from(x2);
            let hash: u32 = hash * 51 + u32::from(x1);
            let hash: u32 = hash * 51 + u32::from(x0);
            check(hash, ClearVec::from_slice_le(&[x0, x1, x2, x3]));
          }
        }
      }
    }

    fn check(hash: u32, needle: ClearVec) -> () {
      let mut cur: Option<ClearVec> = Some(get_base_solution_high(hash));
      while let Some(vec) = cur {
        if vec == needle {
          return;
        } else {
          cur = vec.next_swap_be();
        }
      }
      panic!("Needle not found: {:?}, {:?}", hash, needle);
    }
  }

  #[test]
  fn test_repr_conversions() {
    {
      let cv = ClearVec::from_slice_le(&[0, 9, 43, 29, 17, 37]);
      let low = cv.low_repr();
      let high = cv.high_repr();
      assert_eq!(low, ClearVec::from_slice_le(&[0, 9, 43, 29, 17, 37]));
      assert_eq!(high, ClearVec::from_slice_le(&[51, 59, 42, 29, 17, 37]));
    }
  }

  #[test]
  #[ignore]
  fn test_all_repr_conversions() {
    for hash in 0..(63 * (1 + 51 * (1 + 51 * (1 + 51)))) {
      let low = get_base_solution_low(hash);
      let high = get_base_solution_high(hash);
      assert_eq!(high.low_repr(), low);
      assert_eq!(low.high_repr(), high);
    }
  }

  #[test]
  #[ignore]
  fn test_solve_with_length_completness() {
    use std::iter::FromIterator;

    let expected_set: HashSet<ClearVec> = HashSet::from_iter(vec![
      ClearVec::from_str("e9uWIC"),
      ClearVec::from_str("e9uWIw"),
      ClearVec::from_str("fkuWIC"),
      ClearVec::from_str("fkuWIw"),
      ClearVec::from_str("lvQVs2"),
      ClearVec::from_str("lvQVsW"),
      ClearVec::from_str("lvQVtd"),
      ClearVec::from_str("rHnT2x"),
      ClearVec::from_str("rHnT2D"),
      ClearVec::from_str("rHnUdx"),
      ClearVec::from_str("rHnUdD"),
      ClearVec::from_str("xSJSMX"),
      ClearVec::from_str("xSJSM3"),
      ClearVec::from_str("D35Rxy"),
      ClearVec::from_str("xSJSNe"),
      ClearVec::from_str("D4gRxy"),
      ClearVec::from_str("D35RxE"),
      ClearVec::from_str("Ee5Rxy"),
      ClearVec::from_str("D4gRxE"),
      ClearVec::from_str("EfgRxy"),
      ClearVec::from_str("Ee5RxE"),
      ClearVec::from_str("KqCP6Y"),
      ClearVec::from_str("EfgRxE"),
      ClearVec::from_str("KqCP7$"),
      ClearVec::from_str("KqCP64"),
      ClearVec::from_str("KqCQhY"),
      ClearVec::from_str("KqCP7f"),
      ClearVec::from_str("KqCQi$"),
      ClearVec::from_str("KqCQh4"),
      ClearVec::from_str("QBYORz"),
      ClearVec::from_str("KqCQif"),
      ClearVec::from_str("QC$ORz"),
      ClearVec::from_str("QBYORF"),
      ClearVec::from_str("WNvNBZ"),
      ClearVec::from_str("QC$ORF"),
      ClearVec::from_str("WNvNCa"),
      ClearVec::from_str("WNvNB5"),
      ClearVec::from_str("2YRMmA"),
      ClearVec::from_str("WNvNCg"),
      ClearVec::from_str("3$RMmA"),
      ClearVec::from_str("2YRMmG"),
      ClearVec::from_str("8_oKV0"),
      ClearVec::from_str("3$RMmG"),
      ClearVec::from_str("8_oKWb"),
      ClearVec::from_str("8_oKV6"),
      ClearVec::from_str("9loKV0"),
      ClearVec::from_str("8_oKWh"),
      ClearVec::from_str("9loKWb"),
      ClearVec::from_str("9loKV6"),
      ClearVec::from_str("9loKWh"),
    ].into_iter());

    for x0 in 0..N {
      for x1 in 0..N {
        for x2 in 0..N {
          for x3 in 0..N {
            for x4 in 0..N {
              for x5 in 1..N {
                let cv = ClearVec::from_slice_le(&[x0, x1, x2, x3, x4, x5]);
                if (cv.obf_hash() == 3) != expected_set.contains(&cv) {
                  panic!("Failed on {:?}", cv);
                }
              }
            }
          }
        }
      }
    }
  }

  #[test]
  fn test_get_pow2_31_lte_len_low() {
    assert_eq!(get_pow2_31_lte_len_low(0), ClearVec::from_slice_le(&[]));
    assert_eq!(get_pow2_31_lte_len_low(1), ClearVec::from_slice_le(&[]));
    assert_eq!(get_pow2_31_lte_len_low(2), ClearVec::from_slice_le(&[]));
    assert_eq!(get_pow2_31_lte_len_low(3), ClearVec::from_slice_le(&[]));
    assert_eq!(get_pow2_31_lte_len_low(4), ClearVec::from_slice_le(&[]));
    assert_eq!(get_pow2_31_lte_len_low(5), ClearVec::from_slice_le(&[]));
    assert_eq!(get_pow2_31_lte_len_low(6), ClearVec::from_slice_le(&[]));
    assert_eq!(get_pow2_31_lte_len_low(7), ClearVec::from_slice_le(&[4, 29, 40, 22, 40, 49]));
    assert_eq!(get_pow2_31_lte_len_low(8), ClearVec::from_slice_le(&[30, 17, 17, 34, 23, 45, 50]));
  }
}
