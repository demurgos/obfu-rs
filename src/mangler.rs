use crate::clear_vec::ClearVec;
use crate::convert::{clear_to_vec, OBF_ID_OTHER, OBF_ID_START};

pub fn mangle(str: &str) -> String {
  let v = clear_to_vec(str);
  let h = hash_vec(&v);
  return encode_hash(h);
}

pub fn hash_vec(v: &[u8]) -> u32 {
  let mut hash: u32 = 0;
  for x in v {
    hash = hash.wrapping_mul(51u32).wrapping_add(*x as u32);
  }
  abs_i31(hash)
}

pub fn hash_clear_vec(cv: ClearVec) -> u32 {
  let mut hash: u32 = 0;
  for i in (0..usize::from(cv.len)).rev() {
    hash = hash.wrapping_mul(51u32).wrapping_add(u32::from(cv.bytes[i]));
  }
  abs_i31(hash)
}

/// Converts to an i31, computes the absolute value and transmutes as u30
/// u31::MIN is mapped to 0 (instead of itself as in OCaml)
pub fn abs_i31(x: u32) -> u32 {
  let sign_bit: u32 = (x & 0x40000000u32) >> 30;
  (x ^ 0u32.wrapping_sub(sign_bit)).wrapping_add(sign_bit) & 0x3fffffffu32
}

pub fn encode_hash(mut hash: u32) -> String {
  let mut result: String = String::new();
  while hash > 0 {
    let chars: &str = if result.len() == 0 { OBF_ID_START } else { OBF_ID_OTHER };
    let index: usize = (hash % chars.len() as u32) as usize;
    result.push(chars.chars().nth(index).unwrap());
    hash = (hash - index as u32) / chars.len() as u32;
  }
  return result;
}

#[cfg(test)]
mod tests {
  use super::*;

  #[test]
  fn test_hash_vec() {
    {
      let input: Vec<u8> = vec![];
      let expected: u32 = 0;
      assert_eq!(hash_vec(&input), expected);
    }
  }

  #[test]
  fn test_encode_hash() {
    {
      let input: u32 = 1;
      let expected: String = String::from("(");
      assert_eq!(encode_hash(input), expected);
    }
  }

  #[test]
  fn test_mangle() {
    let test_items: Vec<(&str, &str)> = vec![
      ("a", "("),
      ("b", ")"),
      ("c", "*"),
      ("d", "+"),
      ("e", ","),
      ("f", "-"),
      ("g", "0"),
      ("h", "1"),
      ("i", "2"),
      ("j", "3"),
      ("k", "4"),
      ("l", "5"),
      ("m", "6"),
      ("n", "7"),
      ("o", "8"),
      ("p", "9"),
      ("q", ";"),
      ("r", "="),
      ("s", "["),
      ("t", "]"),
      ("v", "}"),
      ("w", " ("),
      ("x", "(("),
      ("y", ")("),
      ("z", "*("),
      ("dt", ";2"),
      ("dx", "{2"),
      ("dy", "}2"),
      ("px", "5R"),
      ("py", "6R"),
      ("bg1", "(9*"),
      ("bg2", ")9*"),
      ("num", "1PC"),
      ("ltmp", "4mJ5"),
      ("main", "0D 6"),
      ("BOSS", "47lJ"),
      ("E_BAD", "272]C"),
      ("Flyer", "[,+]C"),
      ("Green", "9tYHD"),
      ("Label", "[[5RG"),
      ("buffer", "2OnS-("),
      ("Citron", "2L*6s"),
      ("magtutu", "5bjlo"),
      ("tutu_shade", "6,S,["),
    ];
    for (input, expected) in test_items.into_iter() {
      assert_eq!(mangle(input), expected);
    }
  }
}
