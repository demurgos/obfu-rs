mod export;

use std::borrow::Cow;
use std::ffi::OsString;
use exitcode::ExitCode;
use std::fs;
use std::io;
use std::marker::PhantomData;
use std::path::PathBuf;
use clap::Parser;
use serde::Deserialize;
use crate::{ClearVec, encode_hash};

#[derive(Debug, Parser)]
pub struct CliArgs {
  #[clap(subcommand)]
  command: CliCommand,
}

#[derive(Debug, Parser)]
pub enum CliCommand {
  /// Export strings from an SWF file
  #[clap(name = "export")]
  Export(export::Args),
  /// Hash clear identifiers into obfuscated identifiers
  #[clap(name = "hash")]
  Hash(HashArgs),
}

#[derive(Debug, Parser)]
pub struct HashArgs {
  input: Option<PathBuf>,
}

pub fn run<Args, Arg, Stdin, Stdout, Stderr>(
  args: Args,
  stdin: Stdin,
  mut stdout: Stdout,
  mut stderr: Stderr,
) -> ExitCode
  where
    Args: IntoIterator<Item=Arg>,
    Arg: Into<OsString> + Clone,
    Stdin: io::BufRead,
    Stdout: io::Write,
    Stderr: io::Write,
{
  let args: CliArgs = match CliArgs::try_parse_from(args) {
    Ok(args) => args,
    Err(e) => {
      return if e.use_stderr() {
        writeln!(&mut stderr, "{}", e).expect("failed to write to stderr");
        exitcode::USAGE
      } else {
        writeln!(&mut stdout, "{}", e).expect("failed to write to stdout");
        exitcode::OK
      };
    }
  };

  match args.command {
    CliCommand::Export(args) => {
      export::run(args, stdin, stdout, stderr)
    }
    CliCommand::Hash(args) => {
      match &args.input {
        Some(i) => {
          let input = fs::File::open(i).expect("failed to open input file");
          hash(args, io::BufReader::new(input), stdout, stderr)
        },
        None => hash(args, stdin, stdout, stderr)
      }
    }
  }
}

pub fn hash<Stdin, Stdout, Stderr>(
  _args: HashArgs,
  stdin: Stdin,
  mut stdout: Stdout,
  mut stderr: Stderr,
) -> ExitCode
  where
    Stdin: io::BufRead,
    Stdout: io::Write,
    Stderr: io::Write,
{
  let inputs = stdin.jsonl_map::<Cow<'static, str>, ClearVec, _>(|row: Cow<str>| {
    ClearVec::from_str(&row)
  });
  for input in inputs {
    let input: ClearVec = input.expect("failed to read input");
    let output = encode_hash(input.obf_hash());
    writeln!(stdout, "[\"{input}\", \"{output}\"]").expect("failed to write output");
  }
  writeln!(stderr, "done").expect(" failed to write debug");
  exitcode::OK
}

pub trait BufReadExt {
  fn jsonl_map<In, Out, F>(self, map: F) -> JsonlMap<In, Out, Self, F>
    where
      In: for<'a> PoorGat<'a>,
      F: for<'a> FnMut(<In as PoorGat<'a>>::Out) -> Out,
      Self: Sized
  ;
}

pub trait PoorGat<'a> {
  type Out;
}

impl<'a> PoorGat<'a> for u64 {
  type Out = u64;
}

impl<'a> PoorGat<'a> for Cow<'static, str> {
  type Out = Cow<'a, str>;
}

impl<B> BufReadExt for B
  where B: io::BufRead
{
  fn jsonl_map<In, Out, F>(self, map: F) -> JsonlMap<In, Out, Self, F>
    where
      In: for<'a> PoorGat<'a>,
      F: for<'a> FnMut(<In as PoorGat<'a>>::Out) -> Out,
  {
    JsonlMap {
      buf: self,
      str: String::new(),
      map,
      phantom_map: PhantomData,
    }
  }
}

pub struct JsonlMap<In, Out, Buf, F>
  where
    In: for<'a> PoorGat<'a>,
    F: for<'a> FnMut(<In as PoorGat<'a>>::Out) -> Out,
{
  buf: Buf,
  str: String,
  map: F,
  phantom_map: PhantomData<for<'a> fn(<In as PoorGat<'a>>::Out) -> Out>,
}

#[derive(Debug)]
pub enum JsonlMapError {
  Read(io::Error),
  Parse(serde_json::Error),
}

impl<In, Out, Buf, F> Iterator for JsonlMap<In, Out, Buf, F>
  where
    In: for<'a> PoorGat<'a>,
    for<'a> <In as PoorGat<'a>>::Out: Deserialize<'a>,
    Buf: io::BufRead,
    F: for<'a> FnMut(<In as PoorGat<'a>>::Out) -> Out,
{
  type Item = Result<Out, JsonlMapError>;

  fn next(&mut self) -> Option<Result<Out, JsonlMapError>> {
    loop {
      self.str.clear();
      match self.buf.read_line(&mut self.str) {
        Err(e) => return Some(Err(JsonlMapError::Read(e))),
        Ok(0) => return None,
        Ok(_) => {
          // Successful read
        }
      }
      let line = self.str.trim();
      if !line.is_empty() {
        let value: In::Out = match serde_json::from_str(line) {
          Ok(val) => val,
          Err(e) => return Some(Err(JsonlMapError::Parse(e))),
        };
        return Some(Ok((self.map)(value)));
      }
    }
  }
}
