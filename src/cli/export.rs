use std::borrow::Cow;
use std::collections::BTreeSet;
use std::path::PathBuf;
use clap::Parser;
use exitcode::ExitCode;
use std::io;
use std::fs;
use avm1_types::cfg::Action;
use avm1_types::{CatchTarget, PushValue};
use swf_types::{Label, Movie, NamedId, Scene, Tag};

#[derive(Debug, Parser)]
pub struct Args {
  input: Option<PathBuf>,
}

pub fn run<Stdin, Stdout, Stderr>(
  args: Args,
  stdin: Stdin,
  stdout: Stdout,
  stderr: Stderr,
) -> ExitCode
  where
    Stdin: io::BufRead,
    Stdout: io::Write,
    Stderr: io::Write,
{
  match &args.input {
    Some(i) => {
      let input = fs::File::open(i).expect("failed to open input file");
      run_inner(io::BufReader::new(input), stdout, stderr)
    }
    None => run_inner(stdin, stdout, stderr)
  }
}

pub fn run_inner<Stdin, Stdout, Stderr>(
  mut swf_input: Stdin,
  mut stdout: Stdout,
  _stderr: Stderr,
) -> ExitCode
  where
    Stdin: io::BufRead,
    Stdout: io::Write,
    Stderr: io::Write,
{
  let bytes = {
    let mut bytes = Vec::new();
    swf_input.read_to_end(&mut bytes).expect("failed to read swf input");
    bytes
  };
  let movie = swf_parser::parse_swf(&bytes).expect("invalid swf input");
  let mut strings = BTreeSet::new();
  for s in iter_movie_string(&movie) {
    strings.insert(s);
  }
  for s in &strings {
    writeln!(stdout, "{}", serde_json::to_string_pretty(&*s).expect("failed to serialize")).expect("failed to write output");
  }
  exitcode::OK
}

fn iter_movie_string(movie: &Movie) -> SpriteStrings {
  SpriteStrings { tag: None, tags: movie.tags.as_slice().iter() }
}

struct SpriteStrings<'a> {
  tag: Option<TagStrings<'a>>,
  tags: std::slice::Iter<'a, Tag>,
}

impl<'a> Iterator for SpriteStrings<'a> {
  type Item = Cow<'a, str>;

  fn next(&mut self) -> Option<Self::Item> {
    loop {
      if let Some(tag) = &mut self.tag {
        match tag.next() {
          Some(s) => return Some(s),
          None => self.tag = None,
        }
      }
      self.tag = TagStrings::new(self.tags.next()?)
    }
  }
}

enum TagStrings<'a> {
  DefineDynamicText(DefineDynamicTextStrings<'a>),
  DefineSceneAndFrameLabel(DefineSceneAndFrameLabelStrings<'a>),
  DefineSprite(Box<SpriteStrings<'a>>),
  Avm1Cfg(CfgBlockListStrings),
  ExportAssets(ExportAssetsStrings<'a>),
  FrameLabel(FrameLabelStrings<'a>),
  PlaceObject(PlaceObjectStrings<'a>),
}

impl<'a> TagStrings<'a> {
  fn new(tag: &'a Tag) -> Option<Self> {
    match tag {
      Tag::DefineDynamicText(t) => {
        t.variable_name.as_deref().map(|v| Self::DefineDynamicText(DefineDynamicTextStrings { segments: v.split('/').peekable(), last_segment: None }))
      }
      Tag::DefineSceneAndFrameLabelData(t) => {
        let scenes = t.scenes.iter();
        let labels = t.labels.iter();
        Some(Self::DefineSceneAndFrameLabel(DefineSceneAndFrameLabelStrings { scenes, labels }))
      }
      Tag::DefineSprite(t) => {
        Some(Self::DefineSprite(Box::new(SpriteStrings { tag: None, tags: t.tags.iter() })))
      }
      Tag::DoAction(t) => {
        let cfg = avm1_parser::parse_cfg(&t.actions);
        Some(Self::Avm1Cfg(CfgBlockListStrings { block: None, blocks: cfg.blocks.into_iter() }))
      }
      Tag::DoInitAction(t) => {
        let cfg = avm1_parser::parse_cfg(&t.actions);
        Some(Self::Avm1Cfg(CfgBlockListStrings { block: None, blocks: cfg.blocks.into_iter() }))
      }
      Tag::ExportAssets(t) => {
        Some(Self::ExportAssets(ExportAssetsStrings { assets: t.assets.iter() }))
      }
      Tag::FrameLabel(t) => {
        Some(Self::FrameLabel(FrameLabelStrings { name: Some(t.name.as_str()) }))
      }
      Tag::PlaceObject(t) => {
        Some(Self::PlaceObject(PlaceObjectStrings { class_name: t.class_name.as_deref(), name: t.name.as_deref() }))
      }
      _ => None
    }
  }
}

impl<'a> Iterator for TagStrings<'a> {
  type Item = Cow<'a, str>;

  fn next(&mut self) -> Option<Self::Item> {
    match self {
      Self::DefineDynamicText(t) => t.next().map(Cow::Borrowed),
      Self::DefineSceneAndFrameLabel(t) => t.next().map(Cow::Borrowed),
      Self::DefineSprite(t) => t.next(),
      Self::Avm1Cfg(t) => t.next().map(Cow::Owned),
      Self::ExportAssets(t) => t.next().map(Cow::Borrowed),
      Self::FrameLabel(t) => t.next().map(Cow::Borrowed),
      Self::PlaceObject(t) => t.next().map(Cow::Borrowed),
    }
  }
}

struct DefineDynamicTextStrings<'a> {
  segments: std::iter::Peekable<std::str::Split<'a, char>>,
  last_segment: Option<std::str::Split<'a, char>>,
}

impl<'a> Iterator for DefineDynamicTextStrings<'a> {
  type Item = &'a str;

  fn next(&mut self) -> Option<Self::Item> {
    loop {
      if let Some(ls) = &mut self.last_segment {
        return ls.next();
      }
      let next = self.segments.next()?;
      if self.segments.peek().is_some() {
        return Some(next);
      } else {
        self.last_segment = Some(next.split('.'))
      }
    }
  }
}

struct DefineSceneAndFrameLabelStrings<'a> {
  scenes: std::slice::Iter<'a, Scene>,
  labels: std::slice::Iter<'a, Label>,
}

impl<'a> Iterator for DefineSceneAndFrameLabelStrings<'a> {
  type Item = &'a str;

  fn next(&mut self) -> Option<Self::Item> {
    self.scenes.next().map(|s| s.name.as_str()).or_else(|| self.labels.next().map(|l| l.name.as_str()))
  }
}

struct ExportAssetsStrings<'a> {
  assets: std::slice::Iter<'a, NamedId>,
}

impl<'a> Iterator for ExportAssetsStrings<'a> {
  type Item = &'a str;

  fn next(&mut self) -> Option<Self::Item> {
    self.assets.next().map(|asset| asset.name.as_str())
  }
}

struct FrameLabelStrings<'a> {
  name: Option<&'a str>,
}

impl<'a> Iterator for FrameLabelStrings<'a> {
  type Item = &'a str;

  fn next(&mut self) -> Option<Self::Item> {
    self.name.take()
  }
}

struct PlaceObjectStrings<'a> {
  class_name: Option<&'a str>,
  name: Option<&'a str>,
}

impl<'a> Iterator for PlaceObjectStrings<'a> {
  type Item = &'a str;

  fn next(&mut self) -> Option<Self::Item> {
    self.class_name.take().or_else(|| self.name.take())
  }
}

struct CfgBlockListStrings {
  block: Option<CfgBlockStrings>,
  blocks: std::vec::IntoIter<avm1_types::cfg::CfgBlock>,
}

impl CfgBlockListStrings {
  fn new(cfg: avm1_types::cfg::Cfg) -> Self {
    Self {
      block: None,
      blocks: cfg.blocks.into_iter(),
    }
  }
}

impl Iterator for CfgBlockListStrings {
  type Item = String;

  fn next(&mut self) -> Option<Self::Item> {
    loop {
      if let Some(block) = &mut self.block {
        match block.next() {
          Some(s) => return Some(s),
          None => self.block = None,
        }
      }
      let block = self.blocks.next()?;
      self.block = Some(CfgBlockStrings { actions: CfgActionListStrings { action: None, actions: block.actions.into_iter() }, flow: CfgFlowStrings::new(block.flow).map(Box::new)});
    }
  }
}

struct CfgBlockStrings {
  actions: CfgActionListStrings,
  flow: Option<Box<CfgFlowStrings>>,
}

impl Iterator for CfgBlockStrings {
  type Item = String;

  fn next(&mut self) -> Option<Self::Item> {
    self.actions.next().or_else(|| if let Some (f) = &mut self.flow {f.next()} else { None })
  }
}

struct CfgActionListStrings {
  action: Option<CfgActionStrings>,
  actions: std::vec::IntoIter<Action>,
}

impl Iterator for CfgActionListStrings {
  type Item = String;

  fn next(&mut self) -> Option<Self::Item> {
    loop {
      if let Some(action) = &mut self.action {
        match action.next() {
          Some(s) => return Some(s),
          None => self.action = None,
        }
      }
      self.action = CfgActionStrings::new(self.actions.next()?)
    }
  }
}

enum CfgActionStrings {
  ConstantPool(ConstantPoolStrings),
  DefineFunction(DefineFunctionStrings),
  DefineFunction2(DefineFunction2Strings),
  Once(std::iter::Once<String>),
  Push(PushStrings),
}

impl CfgActionStrings {
  fn new(action: Action) -> Option<Self> {
    match action {
      Action::ConstantPool(a) => {
        Some(Self::ConstantPool(ConstantPoolStrings { pool: a.pool.into_iter() }))
      }
      Action::DefineFunction(a) => {
        Some(Self::DefineFunction(DefineFunctionStrings { name: Some(a.name), parameters: a.parameters.into_iter(), body: Box::new(CfgBlockListStrings::new(a.body)) }))
      }
      Action::DefineFunction2(a) => {
        Some(Self::DefineFunction2(DefineFunction2Strings { name: Some(a.name), parameters: a.parameters.into_iter(), body: Box::new(CfgBlockListStrings::new(a.body)) }))
      }
      Action::GotoLabel(a) => {
        Some(Self::Once(std::iter::once(a.label)))
      }
      Action::Push(a) => {
        Some(Self::Push(PushStrings { values: a.values.into_iter() }))
      }
      Action::SetTarget(a) => {
        Some(Self::Once(std::iter::once(a.target_name)))
      }
      _ => None
    }
  }
}

impl Iterator for CfgActionStrings {
  type Item = String;

  fn next(&mut self) -> Option<Self::Item> {
    match self {
      Self::ConstantPool(a) => a.next(),
      Self::DefineFunction(a) => a.next(),
      Self::DefineFunction2(a) => a.next(),
      Self::Once(a) => a.next(),
      Self::Push(a) => a.next(),
    }
  }
}

struct ConstantPoolStrings {
  pool: std::vec::IntoIter<String>,
}

impl Iterator for ConstantPoolStrings {
  type Item = String;

  fn next(&mut self) -> Option<Self::Item> {
    self.pool.next()
  }
}

struct DefineFunctionStrings {
  name: Option<String>,
  parameters: std::vec::IntoIter<String>,
  body: Box<CfgBlockListStrings>,
}

impl Iterator for DefineFunctionStrings {
  type Item = String;

  fn next(&mut self) -> Option<Self::Item> {
    self.name.take().or_else(|| self.parameters.next()).or_else(|| self.body.next())
  }
}

struct DefineFunction2Strings {
  name: Option<String>,
  parameters: std::vec::IntoIter<avm1_types::Parameter>,
  body: Box<CfgBlockListStrings>,
}

impl Iterator for DefineFunction2Strings {
  type Item = String;

  fn next(&mut self) -> Option<Self::Item> {
    self.name.take().or_else(|| self.parameters.next().map(|p| p.name)).or_else(|| self.body.next())
  }
}

struct PushStrings {
  values: std::vec::IntoIter<PushValue>,
}

impl Iterator for PushStrings {
  type Item = String;

  fn next(&mut self) -> Option<Self::Item> {
    while let Some(v) = self.values.next() {
      match v {
        PushValue::String(s) => return Some(s),
        _ => continue,
      }
    }
    None
  }
}

enum CfgFlowStrings {
  Try(TryStrings),
  With(CfgBlockListStrings),
}

impl CfgFlowStrings {
  fn new(flow: avm1_types::cfg::CfgFlow) -> Option<Self> {
    match flow {
      avm1_types::cfg::CfgFlow::Try(f) => {
        let (catch_target, catch) = match f.catch {
          Some(catch) => {
            let target = if let CatchTarget::Variable(s) = catch.target {
              Some(s)
            } else {
              None
            };
            (target, Some(CfgBlockListStrings::new(catch.body)))
          },
          None => (None, None)
        };

        Some(Self::Try(TryStrings {
          r#try: CfgBlockListStrings::new(f.r#try),
          catch_target,
          catch,
          finally: f.finally.map(CfgBlockListStrings::new),
        }))
      }
      avm1_types::cfg::CfgFlow::With(f) => {
        Some(Self::With(CfgBlockListStrings::new(f.body)))
      }
      _ => None
    }
  }
}

impl Iterator for CfgFlowStrings {
  type Item = String;

  fn next(&mut self) -> Option<Self::Item> {
    match self {
      Self::Try(a) => a.next(),
      Self::With(a) => a.next(),
    }
  }
}

struct TryStrings {
  r#try: CfgBlockListStrings,
  catch_target: Option<String>,
  catch: Option<CfgBlockListStrings>,
  finally: Option<CfgBlockListStrings>,
}

impl Iterator for TryStrings {
  type Item = String;

  fn next(&mut self) -> Option<Self::Item> {
    self.r#try.next()
      .or_else(|| self.catch_target.take())
      .or_else(|| if let Some(c) = &mut self.catch {c.next()} else { None })
      .or_else(|| if let Some(c) = &mut self.finally {c.next()} else { None })
  }
}
