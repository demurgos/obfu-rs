use ::std::convert::TryFrom;

pub const OBF_ID_START: &str = " ()*+,-0123456789;=[]{}";
pub const OBF_ID_OTHER: &str = " ()*+,-0123456789;=ABCDEFGHIJKLMNOPQRSTUVWXYZ[]_abcdefghijklmnopqrstuvwxyz{}";

pub fn clear_to_vec(str: &str) -> Vec<u8> {
  return str.chars().map(clear_val).collect();
}

pub fn clear_val(c: char) -> u8 {
  match c {
    '$' => 0,
    c @ 'a'..='z' => 1 + (u8::try_from(u32::from(c)).unwrap_or(255) - u8::from(b'a')),
    c @ 'A'..='Z' => 27 + (u8::try_from(u32::from(c)).unwrap_or(255) - u8::from(b'A')),
    c @ '0'..='9' => 53 + (u8::try_from(u32::from(c)).unwrap_or(255) - u8::from(b'0')),
    '_' => 63,
    _ => 255,
  }
}

pub fn clear_from_val(v: u8) -> char {
  match v {
    0 => '$',
    c @ 1..=26 => char::from(b'a' + (c - 1)),
    c @ 27..=52 => char::from(b'A' + (c - 27)),
    c @ 53..=62 => char::from(b'0' + (c - 53)),
    63 => '_',
    _ => '!',
  }
}

pub fn clear_byte_val(c: u8) -> u8 {
  match c {
    b'$' => 0,
    b'a'..=b'z' => 1 + c - b'a',
    b'A'..=b'Z' => 27 + c - b'A',
    b'0'..=b'9' => 53 + c - b'0',
    b'_' => 63,
    _ => 255,
  }
}

pub fn start_obf_val(c: char) -> u32 {
  match c {
    ' ' => 0,
    '(' => 1,
    ')' => 2,
    '*' => 3,
    '+' => 4,
    ',' => 5,
    '-' => 6,
    c @ '0'..='9' => 7 + (u32::from(c) - u32::from('0')),
    ';' => 17,
    '=' => 18,
    '[' => 19,
    ']' => 20,
    '{' => 21,
    '}' => 22,
    _ => 255,
  }
}

pub fn cont_obf_val(c: char) -> u32 {
  match c {
    ' ' => 0,
    '(' => 1,
    ')' => 2,
    '*' => 3,
    '+' => 4,
    ',' => 5,
    '-' => 6,
    c @ '0'..='9' => 7 + (u32::from(c) - u32::from('0')),
    ';' => 17,
    '=' => 18,
    c @ 'A'..='Z' => 19 + (u32::from(c) - u32::from('A')),
    '[' => 45,
    ']' => 46,
    '_' => 47,
    c @ 'a'..='z' => 48 + (u32::from(c) - u32::from('a')),
    '{' => 74,
    '}' => 75,
    _ => 255,
  }
}

pub fn encode_hash(mut hash: u32) -> String {
  let mut result: String = String::new();
  while hash > 0 {
    let chars: &str = if result.len() == 0 { OBF_ID_START } else { OBF_ID_OTHER };
    let index: usize = (hash % chars.len() as u32) as usize;
    result.push(chars.chars().nth(index).unwrap());
    hash = (hash - index as u32) / chars.len() as u32;
  }
  return result;
}

pub fn decode_hash(hash_str: &str) -> u32 {
  let mut hash: u32 = 0;
  let mut pow: u32 = 1;
  let mut first: bool = true;
  for c in hash_str.chars() {
    if first {
      hash += pow * start_obf_val(c);
      pow *= 23;
      first = false;
    } else {
      hash += pow * cont_obf_val(c);
      pow *= 76;
    }
  }
  hash
}
