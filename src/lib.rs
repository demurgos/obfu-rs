mod convert;
mod demangler;
mod mangler;
mod clear_vec;
pub mod cli;

pub use mangler::mangle;
pub use demangler::{solve_with_prefix, solve_with_suffix, solve, SolveOptions};
pub use clear_vec::ClearVec;
pub use convert::{encode_hash, decode_hash};
