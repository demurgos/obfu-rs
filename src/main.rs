use ::serde_json::{from_reader, Value};
use exitcode::ExitCode;
use rayon::prelude::*;

#[allow(unused_imports)]
use obfu_rs::{ClearVec, decode_hash, solve, SolveOptions};
#[allow(unused_imports)]
use obfu_rs::{solve_with_prefix, solve_with_suffix};

struct Dict {
  lower: Vec<ClearVec>,
  upper: Vec<ClearVec>,
  cap: Vec<ClearVec>,
}

#[allow(unused)]
struct PairDict {
  lower: Vec<ClearVec>,
  upper: Vec<ClearVec>,
  cap: Vec<ClearVec>,
}

#[allow(unused)]
struct MetaDict {
  components: Dict,
  starts: Dict,
  ends: Dict,
}

fn main() {
  let code: ExitCode = obfu_rs::cli::run(
    std::env::args_os(),
    std::io::stdin().lock(),
    std::io::stdout().lock(),
    std::io::stderr(),
  );
  std::process::exit(code);
// //  for solution in solve(
// //    839299328,
// //    SolveOptions {
// //      prefix: ClearVec::from_str(""),
// //      suffix: ClearVec::from_str("_X"),
// //      max_len: 8,
// //    })
// //    .filter(|cv| cv.is_upper_snake_alpha())
// //    {
// //    println!("{}", solution);
// //  }
//
//   for solution in solve(
//      decode_hash(";)"),
//      SolveOptions {
//       prefix: ClearVec::from_str(""),
//       suffix: ClearVec::from_str(""),
//       max_len: 5,
//     })
// //    .filter(|cv| cv.is_camel_alpha())
//   {
//      println!("{}", solution);
//   }
//
// //  for solution in solve(
// //    427526584,
// //    SolveOptions {
// //      prefix: ClearVec::from_str("fl_"),
// //      suffix: ClearVec::from_str(""),
// //      max_len: 7,
// //    })
// //    .filter(|cv| cv.is_camel_alpha())
// //    {
// //    println!("{}", solution);
// //  }
//
//   // let meta_dict = load_meta_dict();
//   // attack_camel3(&meta_dict);
// //   attack_snake3(&meta_dict);
//
// //  let dict = load_dict();
// //  attack_camel2(&dict);
}

#[allow(unused)]
fn attack_camel3(dict: &MetaDict) -> () {
//  let prefix = ClearVec::from_str("fl_");
//  let under = ClearVec::from_str("_");
//   let suffix = ClearVec::from_str("Friction");
//   let suffix = ClearVec::from_str("Frict");
  let suffix = ClearVec::from_str("Factor");

  println!("Start");
  dict.components.lower
    .par_iter()
    .for_each(|first: &ClearVec| {
//      let first_prefix = prefix.concat(*first);
      let first_prefix = *first;
      check(first_prefix.concat(suffix));
      for second in dict.components.cap.iter() {
        let second_prefix = first_prefix.concat(*second);
        check(second_prefix.concat(suffix));
        for third in dict.components.cap.iter() {
          check(second_prefix.concat(*third).concat(suffix));
        }
      }
    });
  println!("Done");
}

#[allow(unused)]
fn attack_pascal3(dict: &MetaDict) -> () {
  let prefix = ClearVec::from_str("initItem");

  println!("Start");
  dict.components.cap
    .par_iter()
    .for_each(|first: &ClearVec| {
      let first_prefix = prefix.concat(*first);
      check(first_prefix);
      for second in dict.components.cap.iter() {
        let second_prefix = first_prefix.concat(*second);
        check(second_prefix);
        for third in dict.components.cap.iter() {
          check(second_prefix.concat(*third));
        }
      }
    });
  println!("Done");
}

#[allow(unused)]
fn attack_snake3(dict: &MetaDict) -> () {
//  let prefix = ClearVec::from_str("hammer_");
  let prefix = ClearVec::from_str("hammer_fx_");
  let under = ClearVec::from_str("_");
//  let suffix = ClearVec::from_str("_X");

  println!("Start");
  dict.components.lower
    .par_iter()
    .for_each(|first: &ClearVec| {
      let first_prefix0 = prefix.concat(*first);
      let first_prefix1 = first_prefix0.concat(under);

      check_snake(first_prefix0);
      check_snake(first_prefix1);

      for second in dict.components.lower.iter() {
        let second_prefix00 = first_prefix0.concat(*second);
        let second_prefix01 = second_prefix00.concat(under);
        let second_prefix10 = first_prefix1.concat(*second);
        let second_prefix11 = second_prefix10.concat(under);

        check_snake(second_prefix00);
        check_snake(second_prefix01);
        check_snake(second_prefix10);
        check_snake(second_prefix11);

        for third in dict.components.lower.iter() {
          let suffixed = *third; // .concat(suffix);

          check_snake(second_prefix00.concat(suffixed));
          check_snake(second_prefix01.concat(suffixed));
          check_snake(second_prefix10.concat(suffixed));
          check_snake(second_prefix11.concat(suffixed));
        }
      }
    });
  println!("Done");
}

#[allow(unused)]
fn attack_upper_snake3(dict: &MetaDict) -> () {
//  let prefix = ClearVec::from_str("BAD_");
  let under = ClearVec::from_str("_");
  let suffix = ClearVec::from_str("_X");

  println!("Start");
  dict.components.upper
    .par_iter()
    .for_each(|first: &ClearVec| {
      let first_prefix0 = *first; // prefix.concat(*first);
      let first_prefix1 = first_prefix0.concat(under);

      check_upper(first_prefix0);
      check_upper(first_prefix1);

      for second in dict.components.upper.iter() {
        let second_prefix00 = first_prefix0.concat(*second);
        let second_prefix01 = second_prefix00.concat(under);
        let second_prefix10 = first_prefix1.concat(*second);
        let second_prefix11 = second_prefix10.concat(under);

        check_upper(second_prefix00);
        check_upper(second_prefix01);
        check_upper(second_prefix10);
        check_upper(second_prefix11);

        for third in dict.components.upper.iter() {
          let suffixed = third.concat(suffix);

          check_upper(second_prefix00.concat(suffixed));
          check_upper(second_prefix01.concat(suffixed));
          check_upper(second_prefix10.concat(suffixed));
          check_upper(second_prefix11.concat(suffixed));
        }
      }
    });
  println!("Done");
}

#[allow(unused)]
fn attack_upper(dict: &Dict) -> () {
  let prefix = ClearVec::from_str("BAD_");
  let under = ClearVec::from_str("_");
  let suffix = ClearVec::from_str("_X");

  println!("Start");
  dict.upper
    .par_iter()
    .for_each(|first_upper: &ClearVec| {
      let first_prefix = prefix.concat(*first_upper);
      let first_prefix_under = first_prefix.concat(under);

      for second in dict.upper.iter() {
        let suffixed = second.concat(suffix);
        check_upper(first_prefix.concat(suffixed));
        check_upper(first_prefix_under.concat(suffixed));
      }
    });
  println!("Done");
}

#[allow(unused)]
fn attack_snake(dict: &Dict) -> () {
  let prefix = ClearVec::from_str("hammer_fx_");
  let under = ClearVec::from_str("_");

  println!("Start");
  dict.lower
    .par_iter()
    .for_each(|first_lower: &ClearVec| {
      let first_prefix = prefix.concat(*first_lower);
      let first_prefix_under = first_prefix.concat(under);

      for second in dict.lower.iter() {
        check_snake(first_prefix.concat(*second));
        check_snake(first_prefix_under.concat(*second));
      }
    });
  println!("Done");
}

#[allow(unused)]
fn attack_camel2(dict: &Dict) -> () {
  let prefix = ClearVec::from_str("_");
//  let suffix = ClearVec::from_str("PortalLinks");

  println!("Start");
  dict.lower
    .par_iter()
    .for_each(|first: &ClearVec| {
      let prefixed: ClearVec = prefix.concat(*first);
      check(prefixed);
//      check(first.concat(suffix));
      for second in dict.cap.iter() {
        check(prefixed.concat(*second));
      }
    });
  println!("Done");
}

#[allow(unused)]
fn attack_pascal(dict: &Dict) -> () {
  println!("Start");
  dict.cap
    .par_iter()
    .for_each(|first_cap: &ClearVec| {
      for second in dict.cap.iter() {
        check_pascal(first_cap.concat(*second));
      }
    });
  println!("Done");
}

#[allow(unused)]
fn check(clear: ClearVec) -> () {
  let old_clear = match clear.obf_hash() {
    39835640 => "initializeItems", // # "1Brx*"
    48720510 => "checkFlash7", // # "(4io+"
    52090809 => "onFlagMapReady", // # "=;15,"
    67379300 => "fl_trackFindReady", // # "=T7d-"
    96010262 => "checkBaseUrl", // # ",lfT2"
    103806534 => "smallAndLargeTextFields", // # "6tKC3"
    114170788 => "initMachineId", // # "00ME4"
    152151997 => "fl_stickSoftRecal", // # ",RE,8"
    153072806 => "LEVEL_READ_LENGTH", // # "2A=58"
    154955368 => "levelConversion", // # "{;MH8"
    160358521 => "updateToSafeCoords", // # "7C-t8"
    169028868 => "getMovementInfo", // # ",OHi9"
    169560048 => "updateSpeedFactor", // # " FHm9"
    170993528 => "decodeLevelTag", // # ",K3x9"
    179071936 => "getScoreItems", // # "1uxh;"
    181833808 => "fl_protectedFields", // # ";vh ="
    211369216 => "lowerDetail", // # "{[+xB"
    218091996 => "summonFireBalls", // # "9Zc[C"
    218548897 => "getSpecialItems", // # "}z0bC"
    221523159 => "mcArrow2", // # "9SSxC"
    247663177 => "lastDebugKey", // # ",iAVF"
    250123835 => "START_OFF_PLAY_TIMER", // # "1OlkF"
    287172263 => "attachEditorBads", // # "67cOJ"
    324739460 => "soccerBallFire", // # "}gO5N"
    345091006 => "OFF_PLAY_TIMER", // # "=Ra6P"
    346476647 => "WARNING_BLINK_COLOR", // # "(7,FP"
    348602409 => "getItemRands", // # "4D,VP"
    350843999 => "getFallHeight", // # "{cxiP"
    352182053 => "countDefined", // # "+4(tP"
    356674295 => "SPEED_BAD_KNOCK", // # "(-pFQ"
    364451326 => "hammer_ghost", // # "[6J0R"
    394391735 => "SCALE_WALL_WALK", // # ";Xi+U"
    427526584 => "fl_endClear", // # "1N5HX"
    435809800 => "hammer_redFire", // # "+1U5Y"
    470617167 => "refreshFriction", // # "}lU]]"
    497609600 => "BAT_MAX_MOVE_X", // # " efCb"
    510504445 => "snapToGround", // # "3[kXc"
    538118794 => "saveAgainTimeoutId", // # "0CaDf"
    559895406 => "onPerfectItemPickUp", // # "5DXPh"
    560357159 => "sprite_CraneCoeur", // # ";P)Th"
    577765792 => "debugPositionLevelGrid", // # "[_,;j"
    580377114 => "itemOut2", // # "6UhRj"
    589031086 => "musicLoadedTimeLeft", // # ",EsGk"
    630102823 => "stickedFactor", // # "{g)Mo"
    635657697 => "updateViewOffset", // # "+Yqyo"
    652170626 => "musicLoadedTime", // # "7F4[q"
    656158430 => "_readNodes", // # "2d5}q"
    664674131 => "hammer_fx_bubble", // # "-IBpr"
    695137906 => "bossOutroTimer", // # ",4Zqu"
    703840784 => "fl_ninjaSkull", // # ",v-gv"
    729090170 => "hammer_fx_usekey", // # "7a49y"
    730025980 => "bindWorld", // # "  8Ey"
    740844520 => "ninjuForbiddenTargetNbr", // # "00aJz"
    745925314 => "onItemScoreBonus", // # "2hssz"
    753163616 => "scaleSticked", // # "+aI[{"
    778369057 => "hammer_mask_darkness_hole", // # "{900(("
    791787682 => "INVERT_KICK_X", // # "2l0N)("
    796086226 => "SPEED_BAD_KILL_HIT", // # "7uPq)("
    803928108 => "getLandTitle", // # " 1S_*("
    812307477 => "hammer_shoot_fram_ball", // # "2mXP+("
    827692290 => "PERFECT_ITEM_ID", // # "06J{,("
    829350512 => "digits", // # "{nq3-("
    839299328 => "BAD_CLIMB_X", // # "*Ii20("
    846407764 => "fl_stickToWall", // # "6z=p0("
    882337763 => "isLevelLoaded", // # "3tfK4("
    886465155 => "hammer_mapicon", // # "-0lm4("
    915163641 => "hammer_blueFire", // # "5}ma7("
    919967598 => "do_stop_sound", // # ",Az18("
    926637936 => "getSpawnFromPortalId", // # "{;6l8("
    930103663 => "stopAllSounds", // # "9vA29("
    930967143 => "getMusicTitle", // # "0uj89("
    937309947 => "isGreenAbricot", // # "2TTp9("
    941885782 => "fl_ninjaHeart", // # ";AyC;("
    985114865 => "hammer_focus_line", // # ")rHYC("
    991880142 => "canApplyTornado", // # "64C=D("
    996083850 => "TRIGGERED_EXPLODE_TIMER", // # "4(wbD("
    1004690661 => "initializeLinks", // # "8ofTE("
    1013455352 => "ITEM_UNDEFINED_DIAMOND", // # "+ydJF("
    1023084616 => "exitPointerMC", // # "[d5GG("
    1043549203 => "BAT_MAX_MOVE", // # "419II("
    1046438492 => "isAlreadyHit", // # ";(zaI("
    1054751754 => "crystalWordShadow", // # "{tVQJ("
    1069843446 => "hammer_fx_electricity", // # "3Z3zK("
    _ => return (),
  };
  println!("{} ({})", clear, old_clear);
}

#[allow(unused)]
fn check_upper(clear: ClearVec) -> () {
  let old_clear: &str = match clear.obf_hash() {
//    153072806 => "LEVEL_READ_LENGTH",
//    250123835 => "START_OFF_PLAY_TIMER",
//    346476647 => "WARNING_BLINK_COLOR",
//    345091006 => "OFF_PLAY_TIMER",
//    394391735 => "CRAWLER_WALK_SCALE",
//    356674295 => "SPEED_BAD_KNOCK",
//    791787682 => "INVERT_KICK_X",
//    796086226 => "SPEED_BAD_KILL_HIT",
//    996083850 => "TRIGGERED_EXPLODE_TIMER",
//    827692290 => "PERFECT_ITEM_ID",
    839299328 => "BAD_CLIMB_X",
//    1013455352 => "ITEM_UNDEFINED_DIAMOND",
    1043549203 => "batFlyingRange",
    497609600 => "batFlyingRangeX",
    _ => return (),
  };
  println!("{} ({})", clear, old_clear);
}

#[allow(unused)]
fn check_pascal(clear: ClearVec) -> () {}

#[allow(unused)]
fn check_snake(clear: ClearVec) -> () {
  let old_clear = match clear.obf_hash() {
    364451326 => "hammer_ghost",
    435809800 => "hammer_redFire",
    560357159 => "sprite_CraneCoeur",
    729090170 => "hammer_fx_usekey",
    812307477 => "hammer_shoot_fram_ball",
    886465155 => "hammer_mapicon",
    1069843446 => "hammer_fx_electricity",
    664674131 => "hammer_fx_bubble",
    778369057 => "hammer_mask_darkness_hole",
    915163641 => "hammer_blueFire",
    985114865 => "hammer_focus_line",
    704894568 => "hammer_editor_item_label",
    _ => return (),
  };
  println!("{} ({})", clear, old_clear);
}

#[allow(unused)]
fn load_dict() -> Dict {
  let dict_file = ::std::fs::File::open("dict.json").unwrap();
  let dict_reader = ::std::io::BufReader::new(dict_file);
  let raw_dict: Value = from_reader(dict_reader).unwrap();
  dict_from_raw_json(raw_dict)
}

fn dict_from_raw_json(raw_dict: Value) -> Dict {
  let lower: Vec<ClearVec> = match raw_dict {
    Value::Array(arr) => {
      arr.into_iter().map(|item| {
        match item {
          Value::String(s) => ClearVec::from_str(&s),
          _ => panic!("NotAString"),
        }
      }).collect()
    }
    _ => panic!("NotAnArray"),
  };

  let upper: Vec<ClearVec> = lower.iter().map(|x| x.upper()).collect();
  let cap: Vec<ClearVec> = lower.iter().map(|x| x.capitalize()).collect();

  return Dict { lower, upper, cap };
}

#[allow(unused)]
fn load_meta_dict() -> MetaDict {
  let dict_file = ::std::fs::File::open("meta-dict.json").unwrap();
  let dict_reader = ::std::io::BufReader::new(dict_file);
  let raw_dict: Value = from_reader(dict_reader).unwrap();
  meta_dict_from_raw_json(raw_dict)
}

fn meta_dict_from_raw_json(raw_dict: Value) -> MetaDict {
  match raw_dict {
    Value::Object(mut obj) => {
      let components = obj.remove("components").expect("Missing `components`");
      let starts = obj.remove("starts").expect("Missing `starts`");
      let ends = obj.remove("ends").expect("Missing `ends`");

      let components = dict_from_raw_json(components);
      let starts = dict_from_raw_json(starts);
      let ends = dict_from_raw_json(ends);

      MetaDict { components, starts, ends }
    }
    _ => panic!("NotAnArray"),
  }
}
