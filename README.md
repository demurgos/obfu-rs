# Obfu-rs

Mangler and demangler for [Obfu (AVM1)](http://tech.motion-twin.com/obfu.html).

This project was created to help with the deobfuscation of the last Hammerfest identifiers. It was not initially written with the intent to be reused, but the code may still be useful.

## Usage

To build and run the application, use the following command:

```bash
cargo run --bin obfu-rs --release
```

## Overview

This library contains the mangling algorithm of obfu. Mangling is the operation converting the clear identifier (`clear`) to an obfuscated identifier (`obf`).

The diagram below provides an overview of the algorithm and representations used in the library/

- `clear` represents a clear identifier name (as written in the source code).
- `ClearVec` is an optimized representation of `clear` as a fixed-size array of 32 6-bit unsigned integers.
- `hash` is a 30-bit unsigned integer derived from `ClearVec`.
- `obf` is the obfuscated identifier name.

```
+------------+    from_str       +-----------+
| clear: str | <---------------> | ClearVec  |
+------------+    to_string      +-----------+
                                       |
                                       | hash_clear_vec
                                       v
+------------+    encode_hash    +-----------+
| obf: str   | <---------------> | hash: u32 |
+------------+    decode_hash    +-----------+

<-> bijection
--> surjection
```

1. `ClearVec::from_str(&clear)` converts from the string representation to the byte-array representation of the clear identifier. It is obtained by mapping characters from the clear name to their numeric value (see `convert::clear_byte_val`).
2. You may optionally use `ClearVec` to apply transformations (convert to lowercase, uppercase, etc.) or check identifiers (e.g. camel case letters only).
3. Hash the `ClearVec` into a 30-bit unsigned integer using `hash_clear_vec`. This is the main step of the mangler: the hash can't be easily reversed: it's mostly a one-way transformation. The hash itself corresponds to evaluating a polynomial at `x = 51`, where the coefficients of the polynomial are the values of the `ClearVec`, and then applying a small transformation corresponding to Ocaml's `abs` function to bring it in the range `[0, 2^30[`.
4. The last step consists in converting the numeric hash to a string: the obfuscated name. There's a fairly simple equivalence between the numeric hash and obfuscated name, you can freely convert between the two.

It's a good idea to pre-process the values to only work on the `ClearVec` and `hash` representations to avoid extra conversions.

## Reversing obfuscated identifiers

Reversing obfuscated identifier means finding a "good" compatible clear identifier. Since the `hash` step is surjective, there are _many_ `clear` identifiers corresponding to a given obfuscated identifier: they all obfuscated to the same value. Among them, good identifiers are the ones most likely to correspond to a name in the source code.

There are two ways to reverse identifiers: dictionary attacks and the solver.

### Dictionary attack

A dictionary attack consists in generating a list of identifiers and hashing them in the hope of finding a collision with an obfuscated name. A good way to generate identifiers is to take a list of identifier components and combine them. For example by trying all the pairs of camel-case identifiers from the list `[url, decode, encode]`, we may find that `urlDecode` and `urlEncode` match some of our hashes: we found two good clear identifiers.

See the `attack_*` in `main.rs` for more details.

### Solver

The second solution is to exhaustively generate all the possible clear values. This will generate a lot of clear names looking like noise, so it's a good idea to apply a filter before printing them.

Most of Motion-Twin's clear identifier are short, so they can be found fairly easily near the first solved identifiers (in the first few thousands).

The solver is pretty good and can support a few constraints to reduce noise. You can set the maximum length, enforce a clear prefix or enforce a clear suffix. Here is an example code generating all the clear names for the hash `152151997` (string `",RE,8"`). It limits the length to `10` and forces the prefix `fl_`.

```rust
for solution in solve(
   152151997,
   SolveOptions {
    prefix: ClearVec::from_str("fl_"),
    suffix: ClearVec::from_str(""),
    max_len: 10,
  })
  .filter(|cv| cv.is_snake_alpha())
{
   println!("{}", solution);
}
```

By letting it run for some time, it will eventually print the clear identifier `fl_elatistck` corresponding to the clear name.
